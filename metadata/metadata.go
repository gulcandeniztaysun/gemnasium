package metadata

import (
	"fmt"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/cyclonedx"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

const (
	// AnalyzerVendor is the vendor/maintainer of the analyzer
	AnalyzerVendor = "GitLab"

	// AnalyzerName is the name of the analyzer
	AnalyzerName = scannerName

	scannerVendor = AnalyzerVendor
	scannerURL    = "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium"

	// scannerID identifies the scanner that generated the report
	scannerID = "gemnasium"

	// scannerName identifies the scanner that generated the report
	scannerName = "Gemnasium"
)

var (
	// AnalyzerVersion is a placeholder value which the Dockerfile will dynamically
	// overwrite at build time with the most recent version from the CHANGELOG.md file
	AnalyzerVersion = "not-configured"

	// ScannerVersion is the semantic version of the scanner
	ScannerVersion = AnalyzerVersion

	// IssueScanner describes the scanner used to find a vulnerability
	IssueScanner = report.Scanner{
		ID:   scannerID,
		Name: scannerName,
	}

	// ReportScanner returns identifying information about a security scanner
	ReportScanner = report.ScannerDetails{
		ID:      scannerID,
		Name:    scannerName,
		Version: ScannerVersion,
		Vendor: report.Vendor{
			Name: scannerVendor,
		},
		URL: scannerURL,
	}

	// ReportAnalyzer returns identifying information about a security scanner
	ReportAnalyzer = ReportScanner

	// AnalyzerUsage provides a one line usage string for the analyzer
	AnalyzerUsage = fmt.Sprintf("%s %s analyzer v%s", AnalyzerVendor, AnalyzerName, AnalyzerVersion)

	// ToolInfo is used in the metadata section of the SBOM
	ToolInfo = cyclonedx.ToolInfo{
		Name:    AnalyzerName,
		Vendor:  AnalyzerVendor,
		Version: AnalyzerVersion,
	}
)
