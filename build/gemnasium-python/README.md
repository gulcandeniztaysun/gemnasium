# gemnasium-python

## About version compatibility

Python package versions are dependent on few things:

- Microarchitecture
- OS
- Python version

To make sure the dependencies in the generated lock file match all three properties of the target system you should update dependencies and lock file using the official Python Docker image. Make sure to use the oldest Python version supported by `gemnasium-python`.

## Generating requirements.txt lock file

To generate the lock file, run the following in the repo root:

```sh
docker run --rm -it --platform "linux/amd64" -v "$PWD:/workspace" -w "/workspace" docker.io/library/python:3.9-slim bash
pip install pip-tools~=7.3
cd build/gemnasium-python
pip-compile requirements.in
```

`pip-tools` settings are defined in ./.pip-tools.toml. It sets all the necessary command line flags automatically. Check the file for what flags are used and why.

## Updating dependencies

Update package versions (or ranges) pinned in requirements.in, then run the following in the repo root:

```sh
docker run --rm -it --platform "linux/amd64" -v "$PWD:/workspace" -w "/workspace" docker.io/library/python:3.9-slim bash
pip install pip-tools~=7.3
cd build/gemnasium-python
pip-compile requirements.in --upgrade
```

If you don't update requirements.in before running the command, `pip-compile` only updates packages if there are newer versions available for the packages that have been pinned using version range (or compatibility notation).
