#!/bin/bash
set -euo pipefail

export DEBIAN_FRONTEND=noninteractive

# grant_write_perm gives write access to root group (gid 0)
# to enable OpenShift support
grant_write_perm() {
  chmod -R g+w "$1"
}

echo ["$(date "+%H:%M:%S")"] "==> Installing packages…"

apt-get clean
apt-get update -q
apt-get install -y --no-install-recommends bsdmainutils git jq curl ca-certificates zstd unzip
apt-get upgrade -y

echo "deb https://deb.debian.org/debian unstable main" | tee -a /etc/apt/sources.list
apt-get update
apt-get install -t unstable -y --no-install-recommends perl zlib1g
apt-get remove -y libperl5.36 perl-modules-5.36
apt-get autoremove -y

# give write access to CA certificates (OpenShift)
mkdir -p /etc/ssl/certs/
touch /etc/ssl/certs/ca-certificates.crt
grant_write_perm /etc/ssl/certs/ca-certificates.crt

# Enable OpenSSH to find relevant known_hosts files in the original home directory, and in the OpenShift compatible home directory we use.
# See https://gitlab.com/gitlab-org/gitlab/-/issues/374571
mkdir -p /root/.ssh
touch /root/.ssh/config
chmod 600 /root/.ssh/config
echo 'UserKnownHostsFile /root/.ssh/known_hosts /tmp/.ssh/known_hosts' >>/root/.ssh/config

echo ["$(date "+%H:%M:%S")"] "==> Installing asdf…"
mkdir -p "$ASDF_DATA_DIR"
git clone --branch "$ASDF_VERSION" https://github.com/asdf-vm/asdf.git "$ASDF_DATA_DIR"
cd "$ASDF_DATA_DIR"
git checkout "$(git describe --abbrev=0 --tags)"

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/asdf.sh
asdf plugin add java
# See issue https://gitlab.com/gitlab-org/gitlab/-/issues/324267
asdf plugin update java 5db87d86d9aa9813602a3db05661cd30332fc2e3
asdf plugin add maven
asdf plugin add sbt
asdf plugin add gradle

asdf install
asdf reshim
asdf current

# shellcheck source=/dev/null
. "$ASDF_DATA_DIR"/plugins/java/set-java-home.bash

echo ["$(date "+%H:%M:%S")"] "==> Cloning gemnasium-db"
git clone --branch "$GEMNASIUM_DB_REF_NAME" "$GEMNASIUM_DB_REMOTE_URL" "$GEMNASIUM_DB_LOCAL_PATH"
grant_write_perm "$GEMNASIUM_DB_LOCAL_PATH"

echo ["$(date "+%H:%M:%S")"] "==> Installing maven plugin"
mvn -q -f /maven-plugin-builder clean install

echo ["$(date "+%H:%M:%S")"] "==> Adding sbt plugins"
mkdir -p "$HOME/.sbt/1.0/plugins"
cp "/sbt-plugin-builder/plugins.sbt" "$HOME/.sbt/1.0/plugins/plugins.sbt"
grant_write_perm "$HOME/.sbt"

# also write sbt plugins configuration in HOME directory of root user
# because this is where it's read from when user is root
mkdir -p "/root/.sbt/1.0/plugins"
cp "/sbt-plugin-builder/plugins.sbt" "/root/.sbt/1.0/plugins/plugins.sbt"

mkdir -p /root/.ivy2/local/net.virtual-void/sbt-dependency-graph/0.10.0-RC1/ivys
mkdir -p /root/.ivy2/local/net.virtual-void/sbt-dependency-graph/0.10.0-RC1/jars
mkdir -p /root/.ivy2/local/org.scala-sbt/sbt-dependency-tree_2.12_1.0/1.6.2/ivys
mkdir -p /root/.ivy2/local/org.scala-sbt/sbt-dependency-tree_2.12_1.0/1.6.2/jars

ln -s /sbt-plugin-builder/sbt-dependency-graph-plugin/ivy.xml /root/.ivy2/local/net.virtual-void/sbt-dependency-graph/0.10.0-RC1/ivys/ivy.xml
ln -s /sbt-plugin-builder/sbt-dependency-graph-plugin/sbt-dependency-graph-0.10.0-RC1.jar /root/.ivy2/local/net.virtual-void/sbt-dependency-graph/0.10.0-RC1/jars/sbt-dependency-graph-0.10.0-RC1.jar
ln -s /sbt-plugin-builder/sbt-dependency-tree-plugin/ivy.xml /root/.ivy2/local/org.scala-sbt/sbt-dependency-tree_2.12_1.0/1.6.2/ivys/ivy.xml
ln -s /sbt-plugin-builder/sbt-dependency-tree-plugin/sbt-dependency-tree-1.6.2.jar /root/.ivy2/local/org.scala-sbt/sbt-dependency-tree_2.12_1.0/1.6.2/jars/sbt-dependency-tree-1.6.2.jar

echo ["$(date "+%H:%M:%S")"] "==> Downloading sbt jars"
sbt sbtVersion scalaVersion -Dsbt.version=1.6.2
sbt sbtVersion scalaVersion -Dsbt.version=1.9.7

echo ["$(date "+%H:%M:%S")"] "==> Installing gradle plugin"
cd /
gradle -p gradle-plugin-builder shadowJar
rm -rf /root/.gradle

echo ["$(date "+%H:%M:%S")"] "==> Beginning cleanup…"
rm -fr /tmp
mkdir -p /tmp
chmod 777 /tmp
chmod +t /tmp

rm -fr "$ASDF_DATA_DIR/docs" \
  "$ASDF_DATA_DIR"/installs/golang/**/go/test \
  "$ASDF_DATA_DIR"/installs/java/**/demo \
  "$ASDF_DATA_DIR"/installs/java/**/man \
  "$ASDF_DATA_DIR"/installs/java/**/sample \
  "$ASDF_DATA_DIR"/installs/python/**/lib/**/test \
  "$ASDF_DATA_DIR"/installs/ruby/**/lib/ruby/gems/**/cache \
  "$ASDF_DATA_DIR"/installs/**/**/share \
  "$ASDF_DATA_DIR"/test \
  "$HOME"/.config/configstore/update-notifier-npm.json \
  "$HOME"/.config/pip/selfcheck.json \
  "$HOME"/.gem \
  "$HOME"/.npm \
  "$HOME"/.wget-hsts \
  /etc/apache2/* \
  /etc/bash_completion.d/* \
  /etc/calendar/* \
  /etc/cron.d/* \
  /etc/cron.daily/* \
  /etc/emacs/* \
  /etc/fonts/* \
  /etc/ldap/* \
  /etc/mysql/* \
  /etc/php/*/apache2/* \
  /etc/profile.d/* \
  /etc/systemd/* \
  /etc/X11/* \
  /lib/systemd/* \
  /usr/lib/apache2/* \
  /usr/lib/systemd/* \
  /usr/lib/valgrid/* \
  /usr/share/applications/* \
  /usr/share/apps/* \
  /usr/share/bash-completion/* \
  /usr/share/calendar/* \
  /usr/share/doc-base/* \
  /usr/share/emacs/* \
  /usr/share/fontconfig/* \
  /usr/share/fonts/* \
  /usr/share/gtk-doc/* \
  /usr/share/icons/* \
  /usr/share/menu/* \
  /usr/share/pixmaps/* \
  /usr/share/themes/* \
  /usr/share/X11/* \
  /usr/share/zsh/* \
  /var/cache/* \
  /var/cache/apt/archives/ \
  /var/lib/apt/lists/* \
  /var/lib/systemd/* \
  /var/log/*

grant_write_perm "/opt"

echo ["$(date "+%H:%M:%S")"] "==> Starting compression…"
zstd_command="/usr/bin/zstd -19 -T0"
cd /opt
tar --use-compress-program "$zstd_command" -cf /opt/asdf.tar.zst asdf &

wait
rm -fr \
  /opt/asdf/
grant_write_perm /opt/asdf.tar.zst

grant_write_perm "$HOME"

echo ["$(date "+%H:%M:%S")"] "==> Done"
