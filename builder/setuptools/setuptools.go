package setuptools

import (
	"os"
	"os/exec"
	"path/filepath"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder/pipdeptree"
)

const (
	pathPython         = "python3"
	pathPipdeptreeJSON = "pipdeptree.json"
)

var _ builder.Builder = (*Builder)(nil)

// Builder generates dependency lists for setuptools projects
type Builder struct{}

// Build generates a dependency list for a setuptools script, and returns its path
func (b Builder) Build(input string) (string, []string, error) {
	// install dependencies using setuptools script
	if err := b.installDeps(input); err != nil {
		return "", nil, err
	}

	// save JSON output of pipdeptree
	output := filepath.Join(filepath.Dir(input), pathPipdeptreeJSON)
	return output, nil, pipdeptree.CreateJSON(output)
}

func (b Builder) installDeps(input string) error {
	dir, filename := filepath.Split(input)
	cmd := exec.Command(pathPython, filename, "install", "--user")
	cmd.Dir = dir
	cmd.Env = os.Environ()
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)
	return err
}

func init() {
	builder.Register("setuptools", &Builder{})
}
