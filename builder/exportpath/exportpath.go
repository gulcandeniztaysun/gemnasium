package exportpath

import (
	"bufio"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/ansi"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder"
)

// ExtractGradle extracts the export paths from the output of the Gemnasium Gradle plugin.
func ExtractGradle(output []byte) ([]string, error) {
	return extract(output, "Writing dependency JSON to")
}

// ExtractGradleHTMLDependencyReport extracts the export paths from the output of the Gradle htmlDependencyReport task.
func ExtractGradleHTMLDependencyReport(output []byte) ([]string, error) {
	paths, err := extract(output, "See the report at: file://")
	if err != nil {
		return nil, err
	}

	for i, path := range paths {
		// Remove the the file scheme prefix.
		// For example, "file:///src/build/reports/project/dependencies/index.html" becomes
		// "/src/build/reports/project/dependencies/index.html".
		fileURI := strings.TrimPrefix(path, "file://")
		reportDir := filepath.Dir(fileURI)
		reportPath, err := findGradleJSReport(reportDir)
		if err != nil {
			return nil, err
		}

		paths[i] = reportPath
	}

	return paths, nil
}

func findGradleJSReport(dir string) (string, error) {
	entries, err := os.ReadDir(dir)
	if err != nil {
		return "", err
	}

	for _, entry := range entries {
		absPath := filepath.Join(dir, entry.Name())

		if filepath.Ext(absPath) == ".js" {
			return absPath, nil
		}
	}

	return "", fmt.Errorf("no gradle build artifacts with extension .js found in %s", dir)
}

// ExtractMaven extracts the export paths from the output of the Gemnasium Maven plugin.
func ExtractMaven(output []byte) ([]string, error) {
	return extract(output, "dependencies have been succesfully dumped into:")
}

// ExtractSbt extracts the export paths from the output of "sbt dependencyDot".
func ExtractSbt(output []byte) ([]string, error) {
	return extract(output, "Wrote dependency graph to")
}

// extract extracts the export paths from the output
// of a builder's Build command. It uses the given pattern
// to detect lines that contain a path to the export file. Paths are expected
// to be at the end of the line.
func extract(output []byte, pattern string) ([]string, error) {
	paths := []string{}
	reader := strings.NewReader(string(output))
	scanner := bufio.NewScanner(reader)
	for scanner.Scan() {
		// Remove all ANSI escape codes that may be included by
		// the build command.
		//
		// Issue: https://gitlab.com/gitlab-org/gitlab/-/issues/430154
		line := ansi.StripAll(scanner.Text())

		// does the line match the given pattern?
		if strings.Contains(line, pattern) {
			// line is a match; export path is the last field of the line
			fields := strings.Fields(line)
			path := strings.Trim(fields[len(fields)-1], "'")
			paths = append(paths, path)
		}
	}
	return paths, scanner.Err()
}

// Split separates the export path of the root project
// from the export paths of its sub-project, if any.
// It returns an ErrNoDependencies error if the root project has no dependency exports.
func Split(paths []string, rootDir string) (rootpath string, subpaths []string, err error) {
	err = builder.ErrNoDependencies
	for _, path := range paths {
		// export for root project is in the root directory
		if filepath.Dir(path) == rootDir {
			// This is the dependency export for root project,
			// so there is no error to be returned.
			rootpath = path
			err = nil
			continue
		}
		// export for a sub-project
		subpaths = append(subpaths, path)
	}
	return rootpath, subpaths, err
}
