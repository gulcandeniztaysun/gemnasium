package golang

import (
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/exec"
	"path/filepath"
	"sort"
	"strings"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/golang"
)

// Assert that the Builder implements the builder.Builder interface.
var _ builder.Builder = (*Builder)(nil)

// Builder holds functionality that generates the list of modules in use by a Go project.
type Builder struct{}

// module holds the data about a Go module
type module struct {
	Main    bool                  // is this the main module?
	Path    string                // module path e.g. gitlab.com/analyzer/gemnasium/v5
	Version string                // module version e.g. v4.0.0
	Error   *struct{ Err string } // error loading module
}

// pkg holds the data about a Go package
type pkg struct {
	Standard   bool                  // is this package part of the standard Go library?
	Module     *module               // info about package's containing module, if any (can be nil)
	ImportPath string                // import path of package
	Error      *struct{ Err string } // error loading package
}

// Build generates a go module dependency list for all modules utilized
// by the main module. Due to constraints in the implementation of Go's
// module resolution, this function requires one of the following to operate
// successfully: internet access, network access to a go module proxy, or
// a pre-loaded modcache. If the builder receives an error it will return a
// NonFatalError.
func (b Builder) Build(input string) (string, []string, error) {
	dir := filepath.Dir(input)
	log.Debugf("Building go modules json dependency list for module in %s", dir)
	// Load the main module's packages.
	pkgs, err := listPackages(dir)
	if err != nil {
		return "", nil, builder.NewNonFatalError("loading packages used by main module", err)
	}

	modules, err := getModules(pkgs)
	if err != nil {
		return "", nil, err
	}

	absOutputPath := filepath.Join(dir, golang.FileGoProjectModulesJSON)
	return writeGoProjectModulesJSONFile(absOutputPath, modules)
}

func listPackages(dir string) ([]pkg, error) {
	var (
		stdout strings.Builder
		stderr strings.Builder
	)
	// We use the -deps flag to list all packages used in a DFS manner.
	// The -test flag includes packages imported in tests e.g. pkg_test
	// or pkg.test binaries. Finally, the -e flag includes the errors
	// encountered while attempting to load a package.
	cmd := exec.Command("go", "list", "-deps", "-test", "-e", "-json=ImportPath,Standard,Module", "./...")
	cmd.Dir = dir
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err := cmd.Run()
	if err != nil {
		return nil, fmt.Errorf("listing go packages cmd: %s dir: %s stderr: %s: %w", cmd, dir, &stderr, err)
	}

	log.Debugf("cmd: %s\nstdout:\n%s\nstderr:\n%s", cmd, &stdout, &stderr)

	r := strings.NewReader(stdout.String())
	dec := json.NewDecoder(r)
	var (
		pkgs          []pkg
		errCollection []string
	)
	for {
		var p pkg
		err := dec.Decode(&p)
		if err == io.EOF {
			break
		}
		if err != nil {
			return nil, fmt.Errorf("decoding go package: %w", err)
		}

		if p.Error != nil {
			errCollection = append(errCollection, fmt.Sprintf("loading go package %s: %s", p.ImportPath, p.Error.Err))
			continue
		}
		// Skip pkgs that belong to the main module
		// i.e. the module of the project we're scanning
		// and any standard library pkgs.
		if p.Standard || (p.Module != nil && p.Module.Main) {
			continue
		}

		pkgs = append(pkgs, p)
	}

	if len(errCollection) > 0 {
		return nil, fmt.Errorf("%s", strings.Join(errCollection, "\n"))
	}

	return pkgs, nil
}

func getModules(pkgs []pkg) ([]parser.Package, error) {
	var (
		seenPackages = make(map[string]bool)
		seenModules  = make(map[string]bool)
		modules      []parser.Package
	)

	for _, p := range pkgs {
		if err := getModulesUsedByPackage(p, seenPackages, seenModules, &modules); err != nil {
			return nil, builder.NewNonFatalError("resolving imported packages", err)
		}
	}

	// If there is no internet connectivity when loading the packages used by the main module,
	// the underlying build tool error will not be returned. This means that local packages
	// will only be returned and the go module list will be empty. If no modules are found,
	// a non-fatal error is returned so that the go.sum parser can be utilized.
	if len(modules) == 0 {
		return nil, builder.NewNonFatalError("could not resolve imported modules", nil)
	}

	sort.Slice(modules, func(i, j int) bool {
		return modules[i].Name < modules[j].Name
	})

	return modules, nil
}

// getModulesUsedByPackage will recursively get all of the modules used by a package. It skips
// previously visited packages and does not create duplicate entries in the modules slice.
func getModulesUsedByPackage(
	p pkg,
	seenPackages map[string]bool,
	seenModules map[string]bool,
	modules *[]parser.Package,
) error {
	// Remove duplicate modules found.
	if seenPackages[p.ImportPath] {
		return nil
	}
	seenPackages[p.ImportPath] = true

	// This removes noise generated by the std library packages. For all packages that do not
	// have a `go.mod` file, the `go` command will synthesize a `go.mod` file that contains
	// the module path. For more information see "https://go.dev/ref/mod#non-module-compat"
	if p.Module == nil {
		return nil
	}

	if p.Module.Error != nil {
		return fmt.Errorf("loading go module %s: %v", p.Module.Path, p.Module.Error)
	}

	// Ignore all packages seen or that are part of the main module since
	// they are part of the standard library.
	if !seenModules[p.Module.Path] && !p.Module.Main {
		// Mark module as seen
		seenModules[p.Module.Path] = true

		// Add module to list
		*modules = append(*modules, parser.Package{Name: p.Module.Path, Version: p.Module.Version})
	}
	return nil
}

func writeGoProjectModulesJSONFile(absOutputPath string, modules []parser.Package) (string, []string, error) {
	log.Debugf("Creating dependency list file %s", absOutputPath)
	f, err := os.Create(absOutputPath)
	if err != nil {
		msg := fmt.Sprintf("creating build artifact file %s", absOutputPath)
		return "", nil, builder.NewNonFatalError(msg, err)
	}

	log.Debugf("Writing dependency list file %s", absOutputPath)
	enc := json.NewEncoder(f)
	enc.SetIndent("", "    ")
	if err := enc.Encode(modules); err != nil {
		msg := fmt.Sprintf("writing go module json dependency to %s", absOutputPath)
		return "", nil, builder.NewNonFatalError(msg, err)
	}
	log.Debugf("Exported dependency list to %s successfully", absOutputPath)

	log.Debugf("Closing file %s", f.Name())
	if err := f.Close(); err != nil {
		return "", nil, fmt.Errorf("closing file %s: %v", f.Name(), err)
	}

	return golang.FileGoProjectModulesJSON, nil, nil
}

func init() {
	builder.Register("go", &Builder{})
}
