package gradle

import (
	"flag"
	"testing"

	"github.com/stretchr/testify/require"
	"github.com/urfave/cli/v2"
)

func TestBuilder(t *testing.T) {
	t.Run("returns the expected default values", func(t *testing.T) {
		b := &Builder{}
		set := flag.NewFlagSet("test", 0)

		tcs := []struct {
			flagName             string
			expectedDefaultValue string
		}{
			{
				flagGradleOpts,
				"",
			},
			{
				flagGradleInitScript,
				"gemnasium-init.gradle",
			},
			{
				flagExperimentalGradleParser,
				"true",
			},
		}

		for _, flag := range b.Flags() {
			flag.Apply(set)
		}

		for _, tc := range tcs {
			f := set.Lookup(tc.flagName)
			require.Equal(t, f.DefValue, tc.expectedDefaultValue)
		}
	})

	t.Run("Configure", func(t *testing.T) {
		tcs := []struct {
			name               string
			expectedTaskName   string
			featureFlagEnabled bool
		}{
			{
				"feature flag disabled",
				"gemnasiumDumpDependencies",
				false,
			},
			{
				"feature flag enabled",
				"htmlDependencyReport",
				true,
			},
		}
		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				b := &Builder{}
				set := flag.NewFlagSet("test", 0)
				set.Bool(flagExperimentalGradleParser, tc.featureFlagEnabled, "")
				cCtx := cli.NewContext(cli.NewApp(), set, nil)

				err := b.Configure(cCtx)
				require.NoError(t, err)
				require.Equal(t, tc.expectedTaskName, b.GradleTaskName)
			})
		}
	})
}
