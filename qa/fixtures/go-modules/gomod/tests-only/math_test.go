package sum

import (
	"fmt"
	"testing"

	minio "github.com/minio/minio/pkg/madmin"
	"github.com/stretchr/testify/assert"
)

func TestAdd(t *testing.T) {
	fmt.Println("The minio package should be detected since we imported the const", minio.DriveStateOffline)
	assert.Equal(t, 2, 1+1, "1+1 is always 2!")
}
