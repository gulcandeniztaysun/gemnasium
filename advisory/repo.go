package advisory

import (
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"strings"
	"unicode"

	log "github.com/sirupsen/logrus"
	"gopkg.in/yaml.v2"
)

// remoteName is the name of the remote used when updating the advisory database.
const remoteName = "origin"

// Repo represents the git repository containing the security advisories,
// as well as the associated GitLab project.
type Repo struct {
	Path      string // Path is the path to the local git clone
	RemoteURL string // RemoteURL is the URL of the remote repo
	WebURL    string // WebURL is the URL of the GitLab project
	RefName   string // RefName is the branch name or commit SHA
}

// SatisfyPackageType ensures that the repo provides advisories
// for the given package type.
func (r Repo) SatisfyPackageType(pkgType string) error {
	// check directory corresponding to package type
	pkgDir := filepath.Join(r.Path, pkgType)
	info, err := os.Stat(pkgDir)
	if err != nil || !info.IsDir() {
		return ErrNoPackageTypeDir{pkgType}
	}

	// look for advisory file
	found := errors.New("advisory file found")
	err = filepath.Walk(pkgDir, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err // error accessing path
		}
		if !info.IsDir() && hasAdvisoryExt(info.Name()) {
			return found // advisory found
		}
		return nil // keep searching for advisories
	})

	switch err {
	case found:
		return nil // advisory found
	case nil:
		return ErrNoAdvisoryForPackageType{pkgType}
	default:
		return err // error accessing path
	}
}

// PackageAdvisories returns the paths of the advisories affecting the given package.
// It excludes directories and files that don't match the file extension of the advisories.
// Paths are relative to the repository.
func (r Repo) PackageAdvisories(pkg Package) ([]string, error) {
	switch pkg.Type {
	case "pypi":
		return pythonAdvisories(r.Path, pkg)
	default:
		return advisories(r.Path, pkg)
	}
}

// Advisory decodes the given advisory.
// Advisory path is relative to the repository path.
func (r Repo) Advisory(relPath string) (*Advisory, error) {
	f, err := os.Open(filepath.Join(r.Path, relPath))
	if err != nil {
		return nil, err
	}
	defer f.Close()

	adv := Advisory{}
	err = yaml.NewDecoder(f).Decode(&adv)
	adv.URL = r.advisoryURL(relPath)
	return &adv, err
}

// advisoryURL returns the web URL of an advisory.
func (r Repo) advisoryURL(relPath string) string {
	if r.WebURL == "" {
		return "" // no URL for this advisory
	}
	return fmt.Sprintf("%s/-/blob/%s/%s", r.WebURL, r.RefName, relPath)
}

// Update updates the local gemnasium-db repository with a git remote.
func (r Repo) Update() error {
	gitCommand := func(args ...string) *exec.Cmd {
		args = append([]string{"-C", r.Path}, args...)
		cmd := exec.Command("git", args...)
		return cmd
	}

	// list of git commands
	argsList := [][]string{}

	// add "git remote set-url" command if needed
	if remoteURL := r.RemoteURL; remoteURL != "" {
		argsList = append(argsList, []string{"remote", "set-url", remoteName, remoteURL})
	}

	// fetch the specified ref from remote
	// --force --tags is used to pull all tags from remote even if they clobber
	// local tags
	argsList = append(argsList, []string{"fetch", "--force", "--tags", remoteName, r.RefName})

	// checkout ref before resetting in order for symbolic-ref check to accurately
	// indicate whether HEAD is at a tag or branch
	argsList = append(argsList, []string{"checkout", r.RefName})

	// run remote update commands
	for _, args := range argsList {
		cmd := gitCommand(args...)
		output, err := cmd.CombinedOutput()
		log.Debugf("%s\n%s", cmd.String(), output)
		if err != nil {
			return err
		}
	}

	// ref is the commit ref to which HEAD will be reset
	ref := r.RefName

	// check whether the current HEAD is a branch
	// if branch then ensure HEAD gets reset to the remote copy
	// by adding remote name to the git ref
	cmd := gitCommand("symbolic-ref", "-q", "HEAD")
	log.Debugf("%s", cmd.String())
	if err := cmd.Run(); err == nil { // branch
		ref = fmt.Sprintf("%s/%s", remoteName, ref)
	}

	// reset HEAD to ref at remote
	cmd = gitCommand("reset", "--hard", ref)
	out, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), out)
	if err != nil {
		return err
	}

	// log the repo's commit id at HEAD
	cmd = gitCommand("rev-parse", "HEAD")
	out, err = cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), out)
	if err != nil {
		return err
	}
	log.Infof("Using commit %s of vulnerability database\n", out)

	return nil
}

func pythonAdvisories(basedir string, pkg Package) ([]string, error) {
	glob, err := pythonGlob(pkg.Slug())
	if err != nil {
		return nil, err
	}

	dirglob := filepath.Join(basedir, glob)
	matches, err := filepath.Glob(dirglob)
	if err != nil {
		return nil, err
	} else if matches == nil {
		return nil, ErrNoPackageDir{Package: pkg}
	} else if len(matches) != 1 {
		return nil, ErrAmbiguousPackageDir{Package: pkg}
	}

	dirEntries, err := os.ReadDir(matches[0])
	if err != nil {
		return nil, err
	}

	pkgSlug, err := filepath.Rel(basedir, matches[0])
	if err != nil {
		return nil, err
	}

	paths := make([]string, 0)
	for _, dirEntry := range dirEntries {
		if !isAdvisory(dirEntry) {
			continue
		}
		paths = append(paths, filepath.Join(pkgSlug, dirEntry.Name()))
	}
	return paths, nil
}

func advisories(basedir string, pkg Package) ([]string, error) {
	dirEntries, err := os.ReadDir(filepath.Join(basedir, pkg.Slug()))
	if _, ok := err.(*os.PathError); ok {
		return nil, ErrNoPackageDir{Package: pkg}
	} else if err != nil {
		return nil, err
	}

	paths := make([]string, 0)
	for _, dirEntry := range dirEntries {
		if !isAdvisory(dirEntry) {
			continue
		}
		paths = append(paths, filepath.Join(pkg.Slug(), dirEntry.Name()))
	}
	return paths, nil
}

func pythonGlob(path string) (string, error) {
	var glob strings.Builder
	var err error

	for _, r := range path {
		if unicode.IsLetter(r) {
			_, err = fmt.Fprintf(&glob, "[%c%c]", unicode.ToLower(r), unicode.ToUpper(r))
		} else if r == '-' || r == '_' || r == '.' {
			_, err = fmt.Fprint(&glob, "[\\-_.]")
		} else {
			_, err = fmt.Fprintf(&glob, "%c", r)
		}
		if err != nil {
			return "", err
		}
	}
	return glob.String(), nil
}

func isAdvisory(dirEntry os.DirEntry) bool {
	return !dirEntry.IsDir() && hasAdvisoryExt(dirEntry.Name())
}

func hasAdvisoryExt(path string) bool {
	switch strings.ToLower(filepath.Ext(path)) {
	case ".yml", ".yaml":
		return true
	default:
		return false
	}
}
