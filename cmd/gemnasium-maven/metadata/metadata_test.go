package metadata_test

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/cmd/gemnasium-maven/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestReportScanner(t *testing.T) {
	want := report.ScannerDetails{
		ID:      "gemnasium-maven",
		Name:    "gemnasium-maven",
		Version: metadata.ScannerVersion,
		Vendor: report.Vendor{
			Name: "GitLab",
		},
		URL: "https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium",
	}
	got := metadata.ReportScanner
	require.Equal(t, want, got)
}
