package main

import (
	"context"
	"encoding/base64"
	"encoding/json"
	"errors"
	"fmt"
	"os"
	"os/exec"
	"path/filepath"
	"time"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/command/v2"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/cacert"
	"gitlab.com/gitlab-org/security-products/analyzers/common/v3/logutil"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/cli/flags"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/cli/sbom"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/convert"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/cyclonedx"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/libfinder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/manifest"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/metadata"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/remediate"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"

	// VRange registration
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/gem"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/golang"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/npm"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/nuget"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/php"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange/semver"

	// Builder registration
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/builder/golang"

	// Parser registration
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/composer"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/conan"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/gemfile"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/golang"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/mvnplugin"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/npm"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/nuget"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/pipdeptree"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/pnpm"
	_ "gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/yarn"
)

const (
	flagRemediate        = "remediate"
	flagRemediateTimeout = "remediate-timeout"
	flagScanLibs         = "scan-libs"

	defaultTimeoutRemediate = 5 * time.Minute
)

func main() {
	app := cli.NewApp()
	app.Name = "analyzer"
	app.Version = metadata.AnalyzerVersion
	app.Authors = []*cli.Author{{Name: metadata.AnalyzerVendor}}
	app.Usage = metadata.AnalyzerUsage

	log.SetFormatter(&logutil.Formatter{Project: metadata.AnalyzerName})
	log.Info(metadata.AnalyzerUsage)

	app.Commands = []*cli.Command{
		findCommand(),
		runCommand(),
		sbom.Command(sbomFlags(), finder.PresetGemnasium, buildProjects),
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatal(err)
	}
}

func findCommand() *cli.Command {
	flags := []cli.Flag{
		&cli.BoolFlag{
			Name:    flagScanLibs,
			Usage:   "Scan vendored libraries",
			EnvVars: []string{"GEMNASIUM_LIBRARY_SCAN_ENABLED"},
			Value:   false,
		},
	}
	flags = append(flags, finder.Flags(finder.PresetGemnasium)...)
	flags = append(flags, libfinder.Flags()...)

	return &cli.Command{
		Name:      "find",
		Aliases:   []string{"f"},
		Usage:     "Find compatible files in a directory",
		ArgsUsage: "[directory]",
		Flags:     flags,
		Action: func(c *cli.Context) error {
			// one argument is expected
			if c.Args().Len() != 1 {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}
			dir := c.Args().Get(0)

			// find dependency files
			projFinder, err := finder.NewFinder(c, finder.PresetGemnasium)
			if err != nil {
				return err
			}
			projects, err := projFinder.FindProjects(dir)
			if err != nil {
				return err
			}
			for _, project := range projects {
				for _, file := range project.Files {
					fmt.Println(project.FilePath(file))
				}
			}

			// find library files
			if c.Bool(flagScanLibs) {
				libFinder, err := libfinder.NewFinder(c)
				if err != nil {
					return err
				}
				libs, err := libFinder.FindLibraries(dir)
				if err != nil {
					return err
				}
				for _, lib := range libs {
					fmt.Println(lib.Path)
				}
			} else {
				log.Warn("skip vendored libraries")
			}

			return nil
		},
	}
}

func runFlags() []cli.Flag {
	flags := []cli.Flag{
		&cli.BoolFlag{
			Name:    flagScanLibs,
			Usage:   "Scan vendored libraries",
			EnvVars: []string{"GEMNASIUM_LIBRARY_SCAN_ENABLED"},
			Value:   false,
		},
		&cli.BoolFlag{
			Name:    flagRemediate,
			Usage:   "Remediate vulnerabilities",
			EnvVars: []string{"DS_REMEDIATE"},
			Value:   true,
		},
		&cli.DurationFlag{
			Name:    flagRemediateTimeout,
			EnvVars: []string{"DS_REMEDIATE_TIMEOUT"},
			Usage:   "Time limit for vulnerabilities auto-remediation",
			Value:   defaultTimeoutRemediate,
		},
	}

	flags = append(flags, sbomFlags()...)
	flags = append(flags, convert.Flags()...)
	return flags
}

func sbomFlags() []cli.Flag {
	// TODO: add support for flagScanLibs as part of https://gitlab.com/gitlab-org/gitlab/-/issues/361604
	return flags.New(
		cacert.NewFlags(),
		finder.Flags(finder.PresetGemnasium),
		libfinder.Flags(),
		scanner.Flags(),
		vrange.Flags(),
	)
}

func runCommand() *cli.Command {
	return &cli.Command{
		Name:    "run",
		Aliases: []string{"r"},
		Usage:   "Run the analyzer on detected project and generate a compatible artifact",
		Flags:   runFlags(),
		Action: func(c *cli.Context) error {
			startTime := time.Now()

			// no args
			if c.Args().Present() {
				cli.ShowSubcommandHelp(c)
				return errors.New("Invalid number of arguments")
			}

			// import CA bundle
			if err := cacert.Import(c, cacert.ImportOptions{}); err != nil {
				return err
			}

			// configure version range resolvers
			if err := vrange.Configure(c); err != nil {
				return err
			}

			// configure builders
			if err := builder.Configure(c); err != nil {
				return err
			}

			// target directory
			targetDir, err := filepath.Abs(c.String(flags.TargetDir))
			if err != nil {
				return err
			}

			// find dependency files
			projFinder, err := finder.NewFinder(c, finder.PresetGemnasium)
			if err != nil {
				return err
			}
			projects, err := projFinder.FindProjects(targetDir)
			if err != nil {
				return err
			}

			// raise warning when there is nothing to scan
			if len(projects) == 0 {
				// mimic search.ErrNotFound error of common/search
				log.Warnf("No match in %s", targetDir)
				return nil
			}

			if err := buildProjects(projFinder.IsExcludedPath, &projects, targetDir); err != nil {
				return err
			}

			// scan dependency files
			scanner, err := scanner.NewScanner(c)
			if err != nil {
				return err
			}
			result, err := scanner.ScanProjects(targetDir, projects)
			if err != nil {
				return err
			}

			// generate SBOMs and manifest
			cycloneDXSBOMs := cyclonedx.ToSBOMs(result, &startTime, metadata.ToolInfo)
			if err := cyclonedx.OutputSBOMs(targetDir, cycloneDXSBOMs); err != nil {
				return err
			}

			if err := manifest.Create(c.String(flags.ArtifactDir), cycloneDXSBOMs, metadata.ReportScanner); err != nil {
				return err
			}

			// scan lib files
			if c.Bool(flagScanLibs) {
				libFinder, err := libfinder.NewFinder(c)
				if err != nil {
					return err
				}
				libs, err := libFinder.FindLibraries(targetDir)
				if err != nil {
					return err
				}
				libFiles, err := scanner.ScanLibs(targetDir, libs)
				if err != nil {
					return err
				}
				result = append(result, libFiles...)
			} else {
				log.Warn("skip vendored libraries")
			}

			// convert to Dependency Scanning report
			convertCfg := convert.Config{
				AnalyzerDetails: metadata.ReportAnalyzer,
				ScannerDetails:  metadata.ReportScanner,
				StartTime:       &startTime,
			}
			vulnReport := convert.NewConverter(c, convertCfg).ToReport(result)

			// TODO: The compare key is removed so that the reports
			// do not include the `cve` field removed in security report
			// schema 15. This is only removed **after** sorting so
			// that the order of vulnerabilities in the expectations
			// is preserved. Once they have been updated, this should
			// be removed as well. There's no way to programmatically
			// update all of the expectations and ensure correctness
			// so this hack is used in its place.
			//
			// Issue: https://gitlab.com/gitlab-org/gitlab/-/issues/427206
			//
			// Compare key must be removed before auto-remediation
			// because removing it changes the vulnerability ID.
			vulnReport.Sort()
			for i := range vulnReport.Vulnerabilities {
				vulnReport.Vulnerabilities[i].CompareKey = ""
			}

			// remediate vulnerabilities
			if c.Bool(flagRemediate) {
				if !isGitClone(targetDir) {
					log.Warn("auto-remediation requires a valid git directory") // don't fail
				} else {
					t := c.Duration(flagRemediateTimeout)
					ctx, cancel := context.WithTimeout(context.Background(), t)
					defer cancel()
					remediateReport(ctx, vulnReport, result...)
				}
			}

			// write Dependency Scanning report
			artifactPath := filepath.Join(c.String(flags.ArtifactDir), command.ArtifactNameDependencyScanning)
			f, err := os.OpenFile(artifactPath, os.O_CREATE|os.O_WRONLY|os.O_TRUNC, 0o644)
			if err != nil {
				return err
			}
			defer f.Close()
			enc := json.NewEncoder(f)
			enc.SetIndent("", "  ")
			return enc.Encode(vulnReport)
		},
	}
}

// affectionEqualsVuln is true when an affection in a file matches a vulnerability of a report.
func affectionEqualsVuln(a scanner.Affection, file scanner.File, v report.Vulnerability) bool {
	if v.Location.File != file.Path {
		return false
	}
	dep := v.Location.Dependency
	if dep == nil {
		// cannot compare w/o a dependency struct
		return false
	}
	if a.Dependency.Name != dep.Name {
		return false
	}
	if a.Dependency.Version != dep.Version {
		return false
	}
	// compare all identifiers starting with the UUID
	aIDs := []string{a.Advisory.UUID}
	aIDs = append(aIDs, a.Advisory.Identifiers...)
	for _, aID := range aIDs {
		for _, vID := range v.Identifiers {
			if aID == vID.Value {
				// one of the identifiers is a match
				return true
			}
		}
	}
	return false
}

// remediateReport attempts to cure affected dependency files, and add remediations to the report.
// The report is also used to get the IDs of vulnerabilities that have been fixed.
func remediateReport(ctx context.Context, vulnReport *report.Report, scanFiles ...scanner.File) {
	for _, file := range scanFiles {
		// vulnID returns the vulnerability ID for an affection.
		vulnID := func(a scanner.Affection) string {
			for _, v := range vulnReport.Vulnerabilities {
				if affectionEqualsVuln(a, file, v) {
					log.Debugf("cured affection refers to vulnerability ID '%s'", v.ID())
					return v.ID()
				}

			}
			log.Errorf("could not find vulnerability ID for %s affecting %s %s in %s",
				a.Advisory.Identifier, a.Dependency.Name, a.Dependency.Version, file.Path)
			return ""
		}

		// attempt to cure
		cures, err := remediate.Remediate(ctx, file)
		switch err {
		case nil:
			// proceed
		case context.DeadlineExceeded:
			// report timeout and proceed
			log.Error("timeout exceeded during auto-remediation")
			return
		default:
			// report error and move on to the next dependency file;
			// dependency scanning must not fail when remediation fails
			log.Print(err)
			continue
		}

		// convert cures to remediations
		rems := []report.Remediation{}
		for _, cure := range cures {
			refs := []report.Ref{}
			for _, affection := range cure.Affections {
				refs = append(refs, report.Ref{ID: vulnID(affection)})
			}
			diff := base64.StdEncoding.EncodeToString(cure.Diff)
			rem := report.Remediation{
				Fixes:   refs,
				Summary: cure.Summary,
				Diff:    diff,
			}
			rems = append(rems, rem)
		}

		// add remediations to report
		vulnReport.Remediations = append(vulnReport.Remediations, rems...)
	}
	return
}

func isGitClone(dir string) bool {
	cmd := exec.Command("git", "-C", dir, "status")
	output, err := cmd.CombinedOutput()
	log.Debugf("%s\n%s", cmd.String(), output)

	return err == nil
}

func buildProjects(isExcludedPathFn finder.IsExcludedPathFunc, projects *[]finder.Project, targetDir string) error {
	var nonFatalError builder.NonFatalError
	for i, p := range *projects {
		reqFile, found := p.RequirementsFile()
		if !found {
			continue
		}
		inputPath := filepath.Join(targetDir, p.FilePath(reqFile))
		log.Debugf("Exporting dependencies for %s", inputPath)
		pkgManager := p.PackageManager.Name
		b := builder.Lookup(pkgManager)
		if b == nil {
			// Not all package managers require or utilize a builder. Returning
			// nil does not indicate an error.
			log.Debugf("No builder found for package manager %s", pkgManager)
			continue
		}
		outputPath, _, err := b.Build(inputPath)
		if err != nil && !errors.As(err, &nonFatalError) {
			return err
		}

		// Do not add the scannable file if a non fatal error occurred while
		// building the project.
		if err != nil && errors.As(err, &nonFatalError) {
			log.Warnf("Non-fatal error encountered while building project: %v", err)
			continue
		}
		if err != nil && !errors.As(err, &nonFatalError) {
			return err
		}
		(*projects)[i].AddScannableFilename(filepath.Base(outputPath))
	}

	return nil
}
