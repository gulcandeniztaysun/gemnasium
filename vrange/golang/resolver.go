package golang

import (
	"errors"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange"
)

// Resolver matches version matching queries for go
type Resolver struct{}

// ErrInconsistentQuery is returned when a query mixes pseudo-versions with semantic versions.
var ErrInconsistentQuery = errors.New("query mixes pseudo-versions with semantic versions")

// Resolve evaluates queries.
func (r Resolver) Resolve(queries []vrange.Query) (*vrange.ResultSet, error) {
	result := make(vrange.ResultSet)
	for _, query := range queries {
		if !IsUniform(query.Range, query.Version) {
			result.Set(query, false, ErrInconsistentQuery)
			continue
		}

		satisfies, err := SatisfiesRangeConstraint(query.Range, query.Version)
		result.Set(query, satisfies, err)
	}
	return &result, nil
}

// TranslateQuery translates the versions in range to pseudo-versions (if required).
// This translation is best-effort. If version-meta information is attached, it will be used. If the information
// is not present, the function tries to distinguish between semantic and pseudo-versions based on the string.
func (r Resolver) TranslateQuery(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
	// ensure version metadata exists
	if versionMeta == nil {
		versionMeta = []advisory.Version{}
	}

	// translate to range with semantic versions, and range with pseudo-versions
	sem, pseudo, err := TranslateRangeConstraint(q.Range, versionMeta)
	if err != nil {
		// if there is an error, use usual vrange Query as a fallback
		return q
	}

	// we have a pseudo-version so we can match the version of the against the translated pseudo-versions
	v := q.Version
	if IsPseudoVersion(v) && len(pseudo) > 0 {
		return vrange.Query{Range: pseudo, Version: v}
	}

	// we have a semantic versions so we can match the version of against the range
	// that only contains semantic versions
	if IsValid(v) && len(sem) > 0 {
		return vrange.Query{Range: sem, Version: v}
	}

	// fallback to original query
	return q
}

var _ vrange.QueryTranslator = (*Resolver)(nil)

func init() {
	vrange.Register("go", &Resolver{})
}
