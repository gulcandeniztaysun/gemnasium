package golang

import (
	"fmt"
	"regexp"
	"strings"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/advisory"
)

// Building pattern to recognize pseudo-version
var (
	versionSegment       = "v0\\.0\\.0"
	timestampSegment     = "\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d\\d"
	shaSegment           = "\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w\\w"
	pseudoVersionPattern = regexp.MustCompile("^" + versionSegment + "\\-" + timestampSegment + "\\-" + shaSegment + "$")
)

// this is a single conjunct '>1.0.1'
var conjunctPattern = regexp.MustCompile("([!=><]+)\\s*([^\\s!=><]+)")

// this pattern describes a conjunction '>1.0.1 <3.5.0'
var conjunctionPattern = regexp.MustCompile("\\s*([!=><]+)\\s*([^\\s!=><]+)\\s*([!=><]+)\\s*([^\\s!=><]+)")

// extract the version from a given conjunct
func extractVersion(conjunct string) string {
	return conjunctPattern.ReplaceAllString(conjunct, "${2}")
}

// extract the operator from a given conjunct
func extractOperator(rangeConstraint string) string {
	return conjunctPattern.ReplaceAllString(rangeConstraint, "${1}")
}

// remove redundant or con
func normalizeConjunction(rangeConstraint string) string {
	return conjunctionPattern.ReplaceAllString(rangeConstraint, "${1}${2} ${3}${4}")
}

func addVersionPrefix(v string) string {
	if !strings.HasPrefix(strings.ToLower(v), "v") {
		return "v" + v
	}
	return v
}

func contains(list []string, x string) bool {
	for _, item := range list {
		if item == x {
			return true
		}
	}
	return false
}

func translateToPseudoVersion(commit advisory.Commit) (string, error) {
	if len(commit.Sha) <= 14 {
		return "", fmt.Errorf("malformed commit SHA")
	}

	pseudoVersion := "v0.0.0-" + commit.Timestamp[0:14] + "-" + commit.Sha[0:12]
	if !IsPseudoVersion(pseudoVersion) {
		return "", fmt.Errorf("could not compute pseudo-version")
	}

	return pseudoVersion, nil
}

// rewritePseudoVersion to something that can be understood by a semantic version matcher
// v0.0.0-<timestamp>-<sha> => v<timestamp>
func rewritePseudoVersion(versionString string) string {
	if IsPseudoVersion(versionString) {
		segments := strings.Split(versionString, "-")
		return fmt.Sprintf("v%s.0.0", segments[1])
	}
	return versionString
}

func isValidNonPseudoVersion(version string) bool {
	return IsValid(version) && !IsPseudoVersion(version)
}

// TranslateRangeConstraint takes a range constraints with version meta information and separates out
// semantic version information and pseudo-version information by means of the version meta information.
// Based on that, this function returns two range constraint strings: one in semantic version format, and
// the other one in pseudo-version format.
func TranslateRangeConstraint(rangeConstraint string, versions []advisory.Version) (string, string, error) {
	var pseudoVersion [][]string
	var semanticVersion [][]string

	var pseudoConjunctOut []string
	var semConjunctOut []string

	versionMetaMap := make(map[string]advisory.Commit)
	for _, ver := range versions {
		versionMetaMap[ver.Number] = ver.Commit
	}

	conjunctAction := func(operator string, constraintVersion string) (bool, error) {
		if isValidNonPseudoVersion(constraintVersion) {
			// version is a valid semantic version
			if ver, ok := versionMetaMap[constraintVersion]; ok {
				// version is not a pseudo-version
				if contains(ver.Tags, constraintVersion) {
					semConjunctOut = append(semConjunctOut, operator+constraintVersion)
				} else {
					// version could well be a pseudo-version
					semConjunctOut = append(semConjunctOut, constraintVersion)
					targetPseudoVersion, err := translateToPseudoVersion(ver)
					if err == nil {
						pseudoConjunctOut = append(pseudoConjunctOut, operator+targetPseudoVersion)
					}
				}
			} else {
				semConjunctOut = append(semConjunctOut, operator+constraintVersion)
			}
		} else {
			// version is a pseudo-version
			if ver, ok := versionMetaMap[constraintVersion]; ok {
				targetPseudoVersion, err := translateToPseudoVersion(ver)
				if err == nil {
					pseudoConjunctOut = append(pseudoConjunctOut, operator+targetPseudoVersion)
				}
			} else if IsPseudoVersion(constraintVersion) {
				// the version has been added as pseudo-version to the affected range
				pseudoConjunctOut = append(pseudoConjunctOut, operator+constraintVersion)
			}
		}
		return false, nil
	}

	disjunctAction := func(disjunct string) (bool, error) {
		if len(pseudoConjunctOut) > 0 {
			pseudoVersion = append(pseudoVersion, pseudoConjunctOut)
		}
		if len(semConjunctOut) > 0 {
			semanticVersion = append(semanticVersion, semConjunctOut)
		}

		pseudoConjunctOut = []string{}
		semConjunctOut = []string{}
		return false, nil
	}

	err := constraintTraversal(rangeConstraint, conjunctAction, disjunctAction)
	if err != nil {
		return "", "", err
	}
	return generateRangeString(semanticVersion), generateRangeString(pseudoVersion), nil
}

func generateRangeString(constraint [][]string) string {
	ret := ""
	for _, firstLayer := range constraint {
		conj := ""
		for _, secondLayer := range firstLayer {
			if len(conj) > 0 {
				conj += " "
			}
			conj += secondLayer
		}
		if len(ret) > 0 {
			ret += "||"
		}
		ret += conj
	}
	return ret
}

func rewriteRangeConstraint(rangeConstraint string) (string, error) {
	var version [][]string
	var versionOut []string

	disjunctAction := func(disjunct string) (bool, error) {
		version = append(version, versionOut)
		versionOut = []string{}
		return false, nil
	}

	conjunctAction := func(operator string, constraintVersion string) (bool, error) {
		if IsPseudoVersion(constraintVersion) {
			newVersion := rewritePseudoVersion(constraintVersion)
			versionOut = append(versionOut, operator+newVersion)
		} else {
			versionOut = append(versionOut, operator+constraintVersion)
		}

		return false, nil
	}

	err := constraintTraversal(rangeConstraint, conjunctAction, disjunctAction)
	if err != nil {
		return "", err
	}

	return generateRangeString(version), nil
}

// SatisfiesRangeConstraint checks whether the version satisfies the version constraints
// as defined by range constraints
func SatisfiesRangeConstraint(rangeConstraint string, testVersion string) (bool, error) {
	if len(rangeConstraint) == 0 || len(testVersion) == 0 {
		return false, fmt.Errorf("no rangeConstraint/version provided")
	}

	testVersion = Canonical(addVersionPrefix(testVersion))

	// ensure that we do not have mixed version ranges at this point
	if !IsUniform(rangeConstraint, testVersion) {
		return false, fmt.Errorf("no support for mixed constraints")
	}

	if IsPseudoVersion(testVersion) {
		rewritten, err := rewriteRangeConstraint(rangeConstraint)
		if err != nil {
			return false, err
		}
		rangeConstraint = rewritten
		testVersion = rewritePseudoVersion(testVersion)
	}
	sat := false
	conjunctSat := true

	disjunctAction := func(disjunct string) (bool, error) {
		if conjunctSat {
			// break look
			sat = conjunctSat
			return true, nil
		}
		// reset for next iteration
		conjunctSat = true
		return false, nil
	}

	conjunctAction := func(operator string, version string) (bool, error) {
		// The result will be 0 if matchVersion = version
		//                   -1 if matchVersion < version
		//                   +1 if matchVersion > version.
		cmp := Compare(testVersion, Canonical(addVersionPrefix(version)))
		switch operator {
		case "=":
			conjunctSat = conjunctSat && cmp == 0
		case "==":
			conjunctSat = conjunctSat && cmp == 0
		case "<":
			conjunctSat = conjunctSat && cmp == -1
		case "<=":
			conjunctSat = conjunctSat && (cmp == -1 || cmp == 0)
		case ">":
			conjunctSat = conjunctSat && cmp == 1
		case ">=":
			conjunctSat = conjunctSat && (cmp == 1 || cmp == 0)
		case "!=":
			conjunctSat = conjunctSat && cmp != 0
		default:
			return false, fmt.Errorf("Malformed operator")
		}
		return false, nil
	}

	err := constraintTraversal(rangeConstraint, conjunctAction, disjunctAction)
	if err != nil {
		return sat, err
	}

	return sat, nil
}

// constraintTraversal implements commonly used logic to split and traverse a
// version constraint ast with two callbacks:
// - conjunctAction is invoked for every conjunct with the operator and the actual conjunct as para
// - disjunctAction is invoked for every disjunct
// we implicitly assume the following grammar
// conjunct := <op><ver>
// disjunct := <conjuct> (<conjuct>)?
// disjunction := <disjunct> (|| <disjunct>)*
// Note that the term disjunct and/or conjunction can be used interchangeably
func constraintTraversal(rangeConstraint string,
	conjunctAction func(string, string) (bool, error),
	disjunctAction func(string) (bool, error),
) error {
	multispace := regexp.MustCompile(`\s+`)
	sanitizedRange := multispace.ReplaceAllString(rangeConstraint, " ")
	for _, conjunction := range strings.Split(sanitizedRange, "||") {
		normalizedConjunction := strings.TrimSpace(normalizeConjunction(conjunction))
		for _, conjunct := range strings.Split(normalizedConjunction, " ") {
			strippedConjunct := multispace.ReplaceAllString(conjunct, "")
			operator := extractOperator(strippedConjunct)
			version := extractVersion(strippedConjunct)
			ret, err := conjunctAction(operator, version)
			if err != nil {
				return err
			}
			if ret {
				return nil
			}
		}
		ret, err := disjunctAction(conjunction)
		if err != nil {
			return err
		}
		if ret {
			return nil
		}
	}
	return nil
}

// IsUniform ensures that ranges are not mixed (i.e., they do not contain semantic versions and pseudo-versions
// together.
func IsUniform(rangeConstraint string, testVersion string) bool {
	pseudoCount := 0
	semCount := 0
	disjunctAction := func(strippedConjunct string) (bool, error) {
		return false, nil
	}
	conjunctAction := func(operator string, version string) (bool, error) {
		if IsPseudoVersion(version) {
			pseudoCount++
		} else {
			semCount++
		}
		return false, nil
	}

	conjunctAction("", testVersion)

	err := constraintTraversal(rangeConstraint, conjunctAction, disjunctAction)
	if err != nil {
		return false
	}
	return pseudoCount == 0 || semCount == 0
}

// IsPseudoVersion checks whether or not the passed version is a valid pseudo-version.
func IsPseudoVersion(version string) bool {
	if pseudoVersionPattern.MatchString(version) {
		return true
	}
	return false
}
