﻿using System;
using System.IO;

namespace Vrange
{
    class Program
    {
        static void Main(string[] args)
        {
            var stderr = new StreamWriter(Console.OpenStandardError());
            const string usage = "vrange <query-json-file>";

            if (args.Length != 1)
            {
                stderr.Write(usage);
                stderr.Flush();
                stderr.Close();
                Environment.Exit(1);
            }

            var inputFile = args[0];

            var constraints = "";
            try
            {
                constraints = File.ReadAllText(inputFile);
            }
            catch (Exception e)
            {
                stderr.WriteLine(e.Message);
                stderr.Flush();
                stderr.Close();
                Environment.Exit(1);
            }

            string solution = "";
            try
            {
                solution = VersionMatcher.checkVersion(constraints);
            }
            catch (VrangeException e)
            {
                stderr.WriteLine(e.Message);
                stderr.Flush();
                stderr.Close();
                Environment.Exit(1);
            }
            
            var stdout = new StreamWriter(Console.OpenStandardOutput());
            stdout.WriteLine(solution);
            stdout.Flush();
            stdout.Close();
            stderr.Flush();
            stderr.Close();
            Environment.Exit(0);
        }
    }
}