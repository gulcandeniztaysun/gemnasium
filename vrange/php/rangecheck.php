#!/usr/bin/env php
<?php

require __DIR__ . '/vendor/autoload.php';

use JsonSchema\SchemaStorage;
use JsonSchema\Validator;
use JsonSchema\Constraints\Factory;
use Composer\Semver\Constraint\MultiConstraint;
use Composer\Semver\Constraint\Constraint;
use Composer\Semver\VersionParser;

class JsonListener extends \JsonStreamingParser\Listener\IdleListener
{
    private $range = null;
    private $version = null;
    private $ctx = '';
    private $dict = [];
    private static $parser = null;
    private $add_comma = false;

    static function init() {
        self::$parser = new VersionParser();
    }

    public function endObject(): void
    {
        if($this->add_comma)
            echo(",\n");

        if($this->range != null && $this->version != null) {
            $this->dict['satisfies'] = ($this->range->matches($this->version) ? true : false);
        }

        echo(json_encode(array($this->dict)[0], JSON_FORCE_OBJECT));

        $this->range = null;
        $this->version = null;
        $this->add_comma = true;
        $this->dict = [];
    }

    public function key(string $key): void
    {
        $this->ctx = $key;
    }

    public function value($value): void
    {
        if($this->ctx == "range"){
            $this->dict['range'] = $value;
            if (strlen($value) == 0) {
                $this->dict['error'] = 'range not specified';
            } else {
                try {
                    $this->range = self::$parser->parseConstraints($value);
                } catch (Exception $e){
                    $this->dict['error'] = $e->getMessage();
                }
            }
        } elseif ($this->ctx == "version") {
            $this->dict['version'] = $value;
            if (strlen($value) == 0) {
                $this->dict['error'] = 'version not specified';
            } else {
                $this->version = self::$parser->parseConstraints("== $value");
            }
        }
        $this->ctx = "";
    }
    
    public function startDocument(): void
    {
        echo("[\n");
    }

    public function endDocument(): void
    {
        echo("\n]");
    }
}

$jsonSchema = <<<'JSON'
{
  "definitions": {},
  "$schema": "http://json-schema.org/draft-07/schema#",
  "$id": "http://example.com/root.json",
  "type": "array",
  "title": "The Root Schema",
  "items": {
    "$id": "#/items",
    "type": "object",
    "title": "The Items Schema",
    "required": [
      "range",
      "version"
    ],
    "properties": {
      "range": {
        "$id": "#/items/properties/range",
        "type": "string",
        "title": "Version Range Definition",
        "pattern": "^(.*)$"
      },
      "error": {
        "$id": "#/items/properties/error",
        "type": "string",
        "title": "Error Message",
        "pattern": "^(.*)$"
      },
      "version": {
        "$id": "#/items/properties/version",
        "type": "string",
        "title": "The Version Schema",
        "pattern": "^(.*)$"
      },
      "satisfies": {
        "$id": "#/items/properties/satisfies",
        "type": "boolean",
        "title": "true if version lies within range, false otherwise",
        "pattern": "^(true|false)$"
      }
    }
  }
}
JSON;

JsonListener::init();

if($argc != 2) {
    echo("Usage: ./rangecheck.php <JSON input file>");
    exit(1);
}

$inputFilename = $argv[1];

if(file_exists($inputFilename) == false) {
    echo("$inputFilename does not exist");
    exit(1);
}

$jsonSchemaObject = json_decode($jsonSchema);
$schemaStorage = new SchemaStorage();
$schemaStorage->addSchema('file://mySchema', $jsonSchemaObject);
$jsonValidator = new Validator(new Factory($schemaStorage));

try {
    $data = json_decode(file_get_contents($argv[1]));
    $jsonValidator->validate($data, $jsonSchemaObject);
    if($jsonValidator->isValid() == false) {
        echo("Malformed JSON file");
        exit(1);
    }
} catch (Exception $e) {
    echo($e->getMessage());
    exit(1);
}

$stream = fopen($inputFilename, 'r');
$listener = new JsonListener();
try {
  $parser = new \JsonStreamingParser\Parser($stream, $listener);
  $parser->parse();
  fclose($stream);
} catch (Exception $e) {
  fclose($stream);
  throw $e;
}
?>
