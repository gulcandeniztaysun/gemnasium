package finder

import "path/filepath"

// Project is a project handled by a package manager or a build tool,
// and detected in a directory.
type Project struct {
	// Dir is the project directory
	Dir string

	// Files are the project files.
	Files []File

	// PackageManager is the package manager or build tool handling the project.
	PackageManager PackageManager
}

// RequirementsFile is the first requirement file, if any
func (p Project) RequirementsFile() (File, bool) {
	for _, file := range p.Files {
		if file.FileType == FileTypeRequirements {
			return file, true
		}
	}
	return File{}, false
}

// ScannableFile is the first scannable file, if any
func (p Project) ScannableFile() (File, bool) {
	for _, file := range p.Files {
		if file.FileType == FileTypeLockFile || file.FileType == FileTypeGraphExport {
			return file, true
		}
	}
	return File{}, false
}

// AddScannableFilename prepends the filename of a scannable file.
func (p *Project) AddScannableFilename(filename string) {
	exportFile := File{Filename: filename, FileType: FileTypeGraphExport}
	p.Files = append([]File{exportFile}, p.Files...)
}

// FilePath returns the path of a given file.
func (p Project) FilePath(f File) string {
	return filepath.Join(p.Dir, f.Filename)
}
