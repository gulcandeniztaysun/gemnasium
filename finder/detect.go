package finder

import (
	"path/filepath"
	"strings"

	log "github.com/sirupsen/logrus"
)

type fileMatch struct {
	Filename       string
	FileType       FileType
	PackageManager PackageManager
}

// NewDetect initializes a new project detection function for the given package managers
func NewDetect(pkgmngrs ...PackageManager) DetectFunc {
	return func(filenames []string) []Project {
		return merge(filter(detectAll(filenames, pkgmngrs)))
	}
}

// merge groups the detected files per package manager, and turns them into projects.
func merge(input []fileMatch) []Project {
	// projMap maps package manager names to projects
	projMap := map[string]*Project{}

	// use map to merge files handled by the same package manager
	for _, match := range input {
		name := match.PackageManager.Name
		proj, _ := projMap[name]
		if proj == nil {
			proj = &Project{
				PackageManager: match.PackageManager,
				Files:          []File{},
			}
			projMap[name] = proj
		}
		file := File{
			Filename: match.Filename,
			FileType: match.FileType,
		}
		proj.Files = append(proj.Files, file)
	}

	// collect projects
	result := []Project{}
	for _, p := range projMap {
		result = append(result, *p)
	}
	return result
}

// filter filters out detected files, and ensures that there's only one package manager per package type.
// For instance, if the detected files match both npm and yarn (same package type),
// it elects one package manager (for instance npm), and it filters out the files detected for the other (for instance yarn).
// When two package managers compete, the one that provides a lock file wins over the one that only provides a lock file,
// otherwise the first detected package manager wins.
func filter(input []fileMatch) []fileMatch {
	// pkgMngrMap maps a detected package type
	// to the name of package manager that's been elected.
	pkgMngrMap := map[PackageType]string{}

	// package manager for which there's a lock file wins
	for _, match := range input {
		if match.FileType == FileTypeLockFile {
			name := match.PackageManager.Name
			pkgType := match.PackageManager.PackageType
			pkgMngrMap[pkgType] = match.PackageManager.Name
			log.Debugf("electing %s for %s based on lock file %s", name, pkgType, match.Filename)
		}
	}

	// otherwise first package manager wins
	result := []fileMatch{}
	for _, match := range input {
		name := match.PackageManager.Name
		pkgType := match.PackageManager.PackageType
		selected, isSet := pkgMngrMap[pkgType]
		if !isSet {
			// this is the first match for this package type,
			// so the corresponding package manager wins
			pkgMngrMap[pkgType] = name
			log.Debugf("Selecting %s for %s because this is the first match", name, pkgType)
			result = append(result, match)
		} else if selected == name {
			// package manager matches the one that's been elected
			pkgMngrMap[pkgType] = name
			result = append(result, match)
		} else {
			// reject match
			log.Debugf("rejecting %s as handled by %s", match.Filename, name)
		}
	}
	return result
}

// detectAll detects all the files supported by the given package managers.
// It iterates the package managers, and compare the files they handle with the given filenames,
// so that the order of the detected files reflects the order of the package managers.
// There might be more than one match for a single file, when it's supported by multiple package managers.
func detectAll(filenames []string, pkgmngrs []PackageManager) []fileMatch {
	result := []fileMatch{}
	for _, pm := range pkgmngrs {
		for _, expect := range pm.Files {
			if strings.HasPrefix(expect.Filename, "*.") {
				// compare file names with expected extension
				expectExt := strings.TrimPrefix(expect.Filename, "*")
				for _, filename := range filenames {
					if filepath.Ext(filename) == expectExt {
						result = append(result, fileMatch{filename, expect.FileType, pm})
					}

				}
			} else {
				// compare file names with expected file name
				for _, filename := range filenames {
					if filename == expect.Filename {
						result = append(result, fileMatch{filename, expect.FileType, pm})
					}
				}
			}
		}
	}
	return result
}
