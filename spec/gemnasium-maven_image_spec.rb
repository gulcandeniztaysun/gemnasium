require "English"

require 'gitlab_secure/integration_test/docker_runner'
require 'gitlab_secure/integration_test/shared_examples/scan_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/report_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/cyclonedx_shared_examples'
require 'gitlab_secure/integration_test/shared_examples/sbom_manifest_shared_examples'
require 'gitlab_secure/integration_test/spec_helper'

describe "running image" do
  let(:fixtures_dir) { "qa/fixtures" }
  let(:expectations_dir) { "qa/expect" }

  def image_name
    ENV.fetch("TMP_IMAGE", "gemnasium-maven:latest")
  end

  context "with no project" do
    before(:context) do
      @output = `docker run -t --rm -w /app #{image_name}`
      @exit_code = $CHILD_STATUS.to_i
    end

    it "shows there is no match" do
      expect(@output).to match(/no match in \/app/i)
    end

    describe "exit code" do
      specify { expect(@exit_code).to be 0 }
    end
  end

  context "with test project" do
    def parse_expected_report(expectation_name, report_filename: "gl-dependency-scanning-report.json")
      path = File.join(expectations_dir, expectation_name, report_filename)
      JSON.parse(File.read path)
    end

    let(:global_vars) do
      {
        "GEMNASIUM_DB_REF_NAME": "v1.2.142",
        "SECURE_LOG_LEVEL": "debug"
      }
    end

    let(:project) { "any" }
    let(:relative_expectation_dir) { project }
    let(:variables) { {} }
    let(:command) { [] }
    let(:script) { nil }
    let(:offline) { false }

    let(:scan) do
      target_dir = File.join(fixtures_dir, project)
      GitlabSecure::IntegrationTest::DockerRunner.run_with_cache(
        image_name, fixtures_dir, target_dir, @output_dir,
        command: command,
        script: script,
        offline: offline,
        variables: global_vars.merge(variables))
    end

    let(:report) { scan.report }

    shared_examples 'a complete scan without vulnerabilities' do
      it_behaves_like 'successful scan'

      describe 'created report' do
        it_behaves_like 'recorded report'
        it_behaves_like 'valid report'
      end
    end

    shared_examples_for 'a complete scan' do
      it_behaves_like "successful scan"

      describe "created report" do
        it_behaves_like "non-empty report"

        it_behaves_like "recorded report" do
          let(:recorded_report) { parse_expected_report(expected_report_dir) }
        end

        it_behaves_like "valid report"
      end
    end

    shared_examples 'an experimental gradle scan' do
      context 'when using DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER', run_parallel: true do
        let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

        it_behaves_like 'a complete scan'

        it 'uses htmlDependencyReport' do
          expect(scan.combined_output).to match(/Task :htmlDependencyReport/)
        end
      end
    end

    context "using maven" do
      let(:project) { "java-maven/default" }
      let(:expected_report_dir) { 'java-maven/default' }

      context 'default configuration', run_parallel: true do
        it_behaves_like 'a complete scan'
      end

      context 'java 8', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=1.8 -Dmaven.compiler.target=1.8 -DskipTests --batch-mode", "DS_JAVA_VERSION": "8" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 11', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=11 -Dmaven.compiler.target=11 -DskipTests --batch-mode", "DS_JAVA_VERSION": "11" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 17', run_parallel: true do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=17 -Dmaven.compiler.target=17 -DskipTests --batch-mode", "DS_JAVA_VERSION": "17" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'java 21', run_parallel: true, build: 'debian' do
        let(:variables) do
          { "MAVEN_CLI_OPTS": "-Dmaven.compiler.source=21 -Dmaven.compiler.target=21 -DskipTests --batch-mode", "DS_JAVA_VERSION": "21" }
        end

        it_behaves_like 'a complete scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'zulu-21.30.15/)
        end
      end

      context 'bugfix for maven versions with release suffix' do
        let(:project) { "java-maven/357660-versions-with-release-suffix" }
        let(:expected_report_dir) { 'java-maven/357660-versions-with-release-suffix' }

        let(:variables) do
          { "GEMNASIUM_DB_LOCAL_PATH": '/app/gemnasium-db-with-fake-vulnerability', "GEMNASIUM_DB_UPDATE_DISABLED": "true" }
        end

        it_behaves_like 'a complete scan'
      end

      context 'multimodule' do
        let(:project) { "java-maven/multimodules/default" }
        let(:expected_report_dir) { "java-maven/multimodules/default" }

        context 'default configuration', run_parallel: true do
          it_behaves_like 'a complete scan'
        end

        context 'project excluded using' do
          let(:expected_report_dir) { "java-maven/multimodules/no-web" }

          context 'module dir name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web" }
            end

            it_behaves_like 'a complete scan'
          end

          context 'exported file name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web/gemnasium-maven-plugin.json" }
            end

            it_behaves_like 'a complete scan'
          end

          context 'build file name', run_parallel: true do
            let(:variables) do
              { "DS_EXCLUDED_PATHS": "web/pom.xml" }
            end

            it_behaves_like 'a complete scan'
          end
        end
      end
    end

    context 'using gradle on RedHat (FIPS)', build: 'redhat' do
      let(:project) { "java-gradle/default" }
      let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: false }) }

      describe "exit code" do
        specify { expect(scan.exit_code).to eq 0 }
      end

      it "logs analyzer name and version" do
        expect(scan.combined_output).to match /\[INFO\].*analyzer v/
      end

      it "logs no error" do
        expect(scan.combined_output).not_to match /\[(ERRO|FATA)\]/
      end

      it "does not create a report" do
        expect(File).not_to exist(scan.report_path)
      end

      it "shows a warning saying that Gradle is not supported" do
        expect(scan.combined_output).to match(/\[WARN\].*Gradle.*not supported/)
      end
    end

    context 'using gradle on Debian', build: 'debian' do
      let(:project) { "java-gradle/default" }
      let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: false }) }
      let(:expected_report_dir) { "java-gradle/default" }

      context 'default configuration', run_parallel: true do
        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'

        it 'ensures that the analyzer uses the expected default version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-17|java-17-openjdk)/)
        end
      end

      context 'with scoped version variations' do
        let(:project) { "java-gradle/scoped-versions" }

        context 'with gemnasium-gradle-plugin' do
          let(:expected_report_dir) { "java-gradle/scoped-versions/with-gemnasium-gradle-plugin" }
          let(:recorded_report) { parse_expected_report(expected_report_dir) }

          it_behaves_like 'a complete scan without vulnerabilities'
        end

        context 'with experimental gradle builtin parser' do
          let(:expected_report_dir) { "java-gradle/scoped-versions/with-experimental-gradle-builtin-parser" }
          let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

          it_behaves_like 'successful scan'

          describe 'created report' do
            it_behaves_like "recorded report" do
              let(:recorded_report) { parse_expected_report(expected_report_dir) }
            end

            it_behaves_like 'valid report'
          end

          it 'uses htmlDependencyReport' do
            expect(scan.combined_output).to match(/Task :htmlDependencyReport/)
          end
        end
      end

      # Gradle rich console output munges the paths of the export.
      # This test verifies that we handle ANSI escape codes correctly
      # and don't regress on this issue.
      #
      # Issue: https://gitlab.com/gitlab-org/gitlab/-/issues/430154
      context 'with rich console output', run_parallel: true do
        let(:variables) { { GRADLE_CLI_OPTS: '--console=rich' } }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'
      end

      context 'when testing GRADLE_PLUGIN_INIT_PATH' do
        let(:variables) { super().merge({ GRADLE_PLUGIN_INIT_PATH: 'gradle-init-with-skipped-configurations.gradle' }) }

        describe "CycloneDX SBOMs" do
          let(:relative_expectation_dir) { "java-gradle/with-custom-gradle-plugin-init-path" }
          let(:relative_sbom_paths) { ["gl-sbom-maven-gradle.cdx.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"

          context 'experimental gradle scan' do
            let(:relative_expectation_dir) { "java-gradle/with-custom-gradle-plugin-init-path-htmldependencyreport" }
            let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

            it_behaves_like 'non-empty CycloneDX files'
            it_behaves_like 'recorded CycloneDX files'
            it_behaves_like 'valid CycloneDX files'
          end
        end
      end

      context 'path to pom excluded', run_parallel: true do
        let(:variables) { {  "DS_EXCLUDED_PATHS": "pom.xml", "DS_JAVA_VERSION": "11" } }
        let(:project) { "java-gradle/with-pom" }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'
      end

      context 'java 8', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 8 } }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-8|java-1.8.0-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 11', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-11|java-11-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '6\.7\.1'/)
        end
      end

      context 'java 17', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 17 } }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version '(adoptopenjdk-17|java-17-openjdk)/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '7\.3\.3'/)
        end
      end

      context 'java 21', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 21 } }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'

        it 'ensures that the analyzer uses the expected pre-installed version of Java' do
          expect(scan.combined_output).to match(/Using java version 'zulu-21.30.15/)
        end

        it 'ensures that the analyzer uses a compatible pre-installed version of gradle' do
          expect(scan.combined_output).to match(/Using gradle version '8\.4'/)
        end
      end

      context 'gradle 5.6', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-5-6" }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'
      end

      context 'gradle 6.7', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-6-7" }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'
      end

      context 'gradle 6.9', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/gradle-6-9" }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'
      end

      context 'gradle 7.3', run_parallel: true do
        let(:project) { "java-gradle/gradle-7-3" }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'
      end

      context 'gradle 8.4', run_parallel: true do
        let(:project) { "java-gradle/gradle-8-4" }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'
      end

      context 'kotlin dsl', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }
        let(:project) { "java-gradle/kotlin-dsl" }
        let(:expected_report_dir) { 'java-gradle/kotlin-dsl' }

        it_behaves_like 'a complete scan'
        it_behaves_like 'an experimental gradle scan'
      end

      # The api configuration is be used to declare dependencies exported by the library API
      # https://docs.gradle.org/current/userguide/java_library_plugin.html#sec:java_library_separation
      context 'api dependencies', run_parallel: true do
        let(:project) { 'java-gradle/api-dependencies' }
        let(:expected_report_dir) { 'java-gradle/api-dependencies' }
        let(:recorded_report) { parse_expected_report(expected_report_dir) }

        context 'with gemnasium-gradle-plugin' do
          # Please see context:
          # https://gitlab.com/groups/gitlab-org/-/epics/12361#note_1761988970
          it_behaves_like 'crashed scan'
        end

        context 'with experimental gradle builtin parser' do
          let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

          it_behaves_like 'a complete scan without vulnerabilities'
        end
      end

      context 'large open source project', run_parallel: true do
        let(:project) { 'java-gradle/thunderbird-android' }
        let(:expected_report_dir) { 'java-gradle/thunderbird-android' }
        let(:recorded_report) { parse_expected_report(expected_report_dir) }

        context 'with gemnasium-gradle-plugin' do
          it_behaves_like 'crashed scan'
        end

        context 'with experimental gradle builtin parser' do
          let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

          it_behaves_like 'a complete scan without vulnerabilities'
        end
      end

      context 'multimodule' do
        let(:project) { "java-gradle/multimodules/default" }
        let(:variables) { super().merge({ DS_JAVA_VERSION: 11 }) }
        let(:expected_report_dir) { "java-gradle/multimodules/default" }

        context 'default configuration', run_parallel: true do
          it_behaves_like 'a complete scan'

          context 'experimental' do
            let(:expected_report_dir) { "java-gradle/multimodules/htmldependencyreport" }
            let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

            include_examples 'an experimental gradle scan'
          end
        end

        context 'customized buildfile names', run_parallel: true do
          let(:project) { "java-gradle/multimodules/subprojects-buildfilename" }

          it_behaves_like 'a complete scan'
        end

        context 'no root dependencies', run_parallel: true do
          let(:project) { "java-gradle/multimodules/no-root-dependencies" }
          let(:expected_report_dir) { "java-gradle/multimodules/no-root-dependencies" }
          let(:vulnerable_files) { ["api/build.gradle", "web/build.gradle"] }

          it_behaves_like 'a complete scan'
        end
      end

      # Added test for circular dependencies in build.gradle with
      # gemnasium-gradle-plugin v1.0.1 fix; unable to use "non-empty report" due
      # to no vulnerabilities, affecting use of "complete scan" shared example.
      # https://gitlab.com/gitlab-org/security-products/analyzers/gemnasium/-/merge_requests/639#note_1792598922
      context "with build containing dependency with circular references", run_parallel: true do
        let(:recorded_report) { parse_expected_report("java-gradle/dependency-with-circular-references/implementation-directive") }

        context "and using the implementation directive" do
          let(:project) { "java-gradle/dependency-with-circular-references/implementation-directive" }

          include_examples 'a complete scan without vulnerabilities'

          context 'experimental' do
            let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

            include_examples 'a complete scan without vulnerabilities'
          end
        end

        context "and using the compile directive which requires gradle <= 6 and Java <= 15" do
          let(:variables) { { "DS_JAVA_VERSION": "11" } }
          let(:project) { "java-gradle/dependency-with-circular-references/compile-directive" }

          include_examples 'a complete scan without vulnerabilities'

          context 'experimental' do
            let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

            include_examples 'a complete scan without vulnerabilities'
          end
        end
      end

      context "cyclone dx" do
        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-maven-gradle.cdx.json"] }

          it_behaves_like "expected CycloneDX metadata tool-name", "Gemnasium"

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"

          context 'experimental gradle scan' do
            let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

            it_behaves_like 'expected CycloneDX metadata tool-name', 'Gemnasium'

            it_behaves_like 'non-empty CycloneDX files'
            it_behaves_like 'recorded CycloneDX files'
            it_behaves_like 'valid CycloneDX files'
          end
        end
      end

      context "using sbom command" do
        let(:script) do
          <<~HERE
          #!/bin/sh

          /analyzer sbom
          HERE
        end

        it "does not create a report" do
          expect(File).not_to exist(scan.report_path)
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-maven-gradle.cdx.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"

          context 'experimental gradle scan' do
            let(:variables) { super().merge({ DS_EXPERIMENTAL_GRADLE_BUILTIN_PARSER: true }) }

            it_behaves_like 'non-empty CycloneDX files'
            it_behaves_like 'recorded CycloneDX files'
            it_behaves_like 'valid CycloneDX files'
          end
        end

        context "and setting ADDITIONAL_CA_CERT_BUNDLE" do
          tests = [
            {
              name: "with an RSA certificate",
              input: <<~EOF,
              -----BEGIN CERTIFICATE-----
              MIIEwTCCAymgAwIBAgIQEBuwEU7c3q23pGqWcDtSITANBgkqhkiG9w0BAQsFADB5
              MR4wHAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJzAlBgNVBAsMHmFub25A
              MmNhYTVkMTRjZWEyIChMaW51eCBVc2VyKTEuMCwGA1UEAwwlbWtjZXJ0IGFub25A
              MmNhYTVkMTRjZWEyIChMaW51eCBVc2VyKTAeFw0yMjExMDgxOTM1NDVaFw0zMjEx
              MDgxOTM1NDVaMHkxHjAcBgNVBAoTFW1rY2VydCBkZXZlbG9wbWVudCBDQTEnMCUG
              A1UECwweYW5vbkAyY2FhNWQxNGNlYTIgKExpbnV4IFVzZXIpMS4wLAYDVQQDDCVt
              a2NlcnQgYW5vbkAyY2FhNWQxNGNlYTIgKExpbnV4IFVzZXIpMIIBojANBgkqhkiG
              9w0BAQEFAAOCAY8AMIIBigKCAYEAsOSblCwOSdS4dS/PJ9qW6HdJG1wKw0GxOlX1
              BPelDyO+7d0AeO3OlqzT0cf+kOC7UfdMtGRWa3miOafxqx+eerdhy8vec67gwT4f
              GYCoSvHq49aQsgG2/U8lG8YKcWwFYGuB5gFyMqdRpHvfzH9cxuPkopqE47i5pCg0
              TFfE7xQEzLY+UHPEd5l7i03Gml6IJ3m1pAHE/RHZOLKj3UxRUk8pey9SAR5x76wd
              3tznK93gW1K7W92JtyzNYSwrXZMwKyUwupWqwMb0fqzgv9VjwjTMxuHXor0zCpIv
              fmgjuv5FiPgCOx+0lderwRR625QbfMJm5Hb01PlfaGFZk7iNvtu+0itWUoPtzcNJ
              ZH2JYc5k/8UcoLgZ61Bjh18ddgIYj4vnmOSclVH6VEC1HB63u2NveIkfn2DMozyQ
              42KzxtZHreDnS7AnrcDFSXQ9GY5hrS38H4+hT0cDe1OHFmIyXk/jJKyDOpBSBra1
              VGlYkG4PBs0BIjitYX2ehuIkolt9AgMBAAGjRTBDMA4GA1UdDwEB/wQEAwICBDAS
              BgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQWBBRCC87b32AICaFDJq6+7WseMj6a
              tjANBgkqhkiG9w0BAQsFAAOCAYEADHWQEeIwLIhO3w4zvPN70jWt/ufpB0vtRzk+
              6u276QxHzm6yO+OrIpHWUbXOQ7sR3FWYJqpP3i4giBtKiHi8eK3FEcuB6zYx7JgP
              SD3b8SXeo+9Jz9sv62rY8pJ6s21nx1gYqqCsCDVtpiwKBAAy43/8hr/7Rg5fXAtM
              0F5tHR6vcScOgBL8ojEhhXzFUar4AVNmJmtmQFI7UEeYeT9FP0Kc1PtQNAETZuJu
              hiXjF5Y1TRS0NjPQxC0yPoHnGn94N68+m5yvSc5tUjH2oyvPCzB0tj3cE3B6hqaI
              CYBJdTTzIQjTLMurIRfjz361/6qAgZVe8VXNPdBBPpNN/20AmuNO463BuvJvRxzZ
              BGJDR312vEyYZTWSroFQNlwoPiwC7JZ2BACA90qckQy31gi+iLlAuyNpiTrbVOFZ
              n3f6VK1qc+2oqupQ6aB3BaqqgZFJEgLFMq91zD6AGp2Mg+/xr+RRs8ItOnCPb0Fb
              K2pldgj2VcEwaLFhtfywosbOzIml
              -----END CERTIFICATE-----
              EOF
              certs: [
                <<~EOF,
                -----BEGIN CERTIFICATE-----
                MIIEwTCCAymgAwIBAgIQEBuwEU7c3q23pGqWcDtSITANBgkqhkiG9w0BAQsFADB5
                MR4wHAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJzAlBgNVBAsMHmFub25A
                MmNhYTVkMTRjZWEyIChMaW51eCBVc2VyKTEuMCwGA1UEAwwlbWtjZXJ0IGFub25A
                MmNhYTVkMTRjZWEyIChMaW51eCBVc2VyKTAeFw0yMjExMDgxOTM1NDVaFw0zMjEx
                MDgxOTM1NDVaMHkxHjAcBgNVBAoTFW1rY2VydCBkZXZlbG9wbWVudCBDQTEnMCUG
                A1UECwweYW5vbkAyY2FhNWQxNGNlYTIgKExpbnV4IFVzZXIpMS4wLAYDVQQDDCVt
                a2NlcnQgYW5vbkAyY2FhNWQxNGNlYTIgKExpbnV4IFVzZXIpMIIBojANBgkqhkiG
                9w0BAQEFAAOCAY8AMIIBigKCAYEAsOSblCwOSdS4dS/PJ9qW6HdJG1wKw0GxOlX1
                BPelDyO+7d0AeO3OlqzT0cf+kOC7UfdMtGRWa3miOafxqx+eerdhy8vec67gwT4f
                GYCoSvHq49aQsgG2/U8lG8YKcWwFYGuB5gFyMqdRpHvfzH9cxuPkopqE47i5pCg0
                TFfE7xQEzLY+UHPEd5l7i03Gml6IJ3m1pAHE/RHZOLKj3UxRUk8pey9SAR5x76wd
                3tznK93gW1K7W92JtyzNYSwrXZMwKyUwupWqwMb0fqzgv9VjwjTMxuHXor0zCpIv
                fmgjuv5FiPgCOx+0lderwRR625QbfMJm5Hb01PlfaGFZk7iNvtu+0itWUoPtzcNJ
                ZH2JYc5k/8UcoLgZ61Bjh18ddgIYj4vnmOSclVH6VEC1HB63u2NveIkfn2DMozyQ
                42KzxtZHreDnS7AnrcDFSXQ9GY5hrS38H4+hT0cDe1OHFmIyXk/jJKyDOpBSBra1
                VGlYkG4PBs0BIjitYX2ehuIkolt9AgMBAAGjRTBDMA4GA1UdDwEB/wQEAwICBDAS
                BgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQWBBRCC87b32AICaFDJq6+7WseMj6a
                tjANBgkqhkiG9w0BAQsFAAOCAYEADHWQEeIwLIhO3w4zvPN70jWt/ufpB0vtRzk+
                6u276QxHzm6yO+OrIpHWUbXOQ7sR3FWYJqpP3i4giBtKiHi8eK3FEcuB6zYx7JgP
                SD3b8SXeo+9Jz9sv62rY8pJ6s21nx1gYqqCsCDVtpiwKBAAy43/8hr/7Rg5fXAtM
                0F5tHR6vcScOgBL8ojEhhXzFUar4AVNmJmtmQFI7UEeYeT9FP0Kc1PtQNAETZuJu
                hiXjF5Y1TRS0NjPQxC0yPoHnGn94N68+m5yvSc5tUjH2oyvPCzB0tj3cE3B6hqaI
                CYBJdTTzIQjTLMurIRfjz361/6qAgZVe8VXNPdBBPpNN/20AmuNO463BuvJvRxzZ
                BGJDR312vEyYZTWSroFQNlwoPiwC7JZ2BACA90qckQy31gi+iLlAuyNpiTrbVOFZ
                n3f6VK1qc+2oqupQ6aB3BaqqgZFJEgLFMq91zD6AGp2Mg+/xr+RRs8ItOnCPb0Fb
                K2pldgj2VcEwaLFhtfywosbOzIml
                -----END CERTIFICATE-----
                EOF
              ],
            },
            {
              name: "with an ECDSA certificate",
              input: <<~EOF,
              -----BEGIN CERTIFICATE-----
              MIICMTCCAdigAwIBAgIQVImsiFfgWr8SMnyZPxfZdzAKBggqhkjOPQQDAjB5MR4w
              HAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJzAlBgNVBAsMHmFub25AMGMz
              YmI4YTVkOGZmIChMaW51eCBVc2VyKTEuMCwGA1UEAwwlbWtjZXJ0IGFub25AMGMz
              YmI4YTVkOGZmIChMaW51eCBVc2VyKTAeFw0yMjExMDMwMDM2MjVaFw0yNTAyMDMw
              MDM2MjVaMFIxJzAlBgNVBAoTHm1rY2VydCBkZXZlbG9wbWVudCBjZXJ0aWZpY2F0
              ZTEnMCUGA1UECwweYW5vbkAwYzNiYjhhNWQ4ZmYgKExpbnV4IFVzZXIpMFkwEwYH
              KoZIzj0CAQYIKoZIzj0DAQcDQgAE/h8ETTXtHFpwa1fp3+7PuILgHjoxYALEHBT1
              AjvbLpZCJy2IIp7ZwEvUHgFVGmpk0DKrEmMKjM+a2p5Wt8BkxKNpMGcwDgYDVR0P
              AQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMB8GA1UdIwQYMBaAFP5qbFWX
              UKQconc8vEmhoXuCD2z+MB8GA1UdEQQYMBaCFCouZ2l0bGFiLmV4YW1wbGUuY29t
              MAoGCCqGSM49BAMCA0cAMEQCIBkBUoO2mjvxOIb6oHH+jFHHzLjKlN2AOkD2obZF
              bzY/AiBhgwVXnhYJlNQWV3GcmhKR6armhCbt612h5RvyqoI30g==
              -----END CERTIFICATE-----
              EOF
              certs: [
                <<~EOF,
                -----BEGIN CERTIFICATE-----
                MIICMTCCAdigAwIBAgIQVImsiFfgWr8SMnyZPxfZdzAKBggqhkjOPQQDAjB5MR4w
                HAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJzAlBgNVBAsMHmFub25AMGMz
                YmI4YTVkOGZmIChMaW51eCBVc2VyKTEuMCwGA1UEAwwlbWtjZXJ0IGFub25AMGMz
                YmI4YTVkOGZmIChMaW51eCBVc2VyKTAeFw0yMjExMDMwMDM2MjVaFw0yNTAyMDMw
                MDM2MjVaMFIxJzAlBgNVBAoTHm1rY2VydCBkZXZlbG9wbWVudCBjZXJ0aWZpY2F0
                ZTEnMCUGA1UECwweYW5vbkAwYzNiYjhhNWQ4ZmYgKExpbnV4IFVzZXIpMFkwEwYH
                KoZIzj0CAQYIKoZIzj0DAQcDQgAE/h8ETTXtHFpwa1fp3+7PuILgHjoxYALEHBT1
                AjvbLpZCJy2IIp7ZwEvUHgFVGmpk0DKrEmMKjM+a2p5Wt8BkxKNpMGcwDgYDVR0P
                AQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMB8GA1UdIwQYMBaAFP5qbFWX
                UKQconc8vEmhoXuCD2z+MB8GA1UdEQQYMBaCFCouZ2l0bGFiLmV4YW1wbGUuY29t
                MAoGCCqGSM49BAMCA0cAMEQCIBkBUoO2mjvxOIb6oHH+jFHHzLjKlN2AOkD2obZF
                bzY/AiBhgwVXnhYJlNQWV3GcmhKR6armhCbt612h5RvyqoI30g==
                -----END CERTIFICATE-----
                EOF
              ],
            },
            {
              name: "with RSA and ECDSA certificates",
              input: <<~EOF,
              -----BEGIN CERTIFICATE-----
              MIIEwTCCAymgAwIBAgIQEBuwEU7c3q23pGqWcDtSITANBgkqhkiG9w0BAQsFADB5
              MR4wHAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJzAlBgNVBAsMHmFub25A
              MmNhYTVkMTRjZWEyIChMaW51eCBVc2VyKTEuMCwGA1UEAwwlbWtjZXJ0IGFub25A
              MmNhYTVkMTRjZWEyIChMaW51eCBVc2VyKTAeFw0yMjExMDgxOTM1NDVaFw0zMjEx
              MDgxOTM1NDVaMHkxHjAcBgNVBAoTFW1rY2VydCBkZXZlbG9wbWVudCBDQTEnMCUG
              A1UECwweYW5vbkAyY2FhNWQxNGNlYTIgKExpbnV4IFVzZXIpMS4wLAYDVQQDDCVt
              a2NlcnQgYW5vbkAyY2FhNWQxNGNlYTIgKExpbnV4IFVzZXIpMIIBojANBgkqhkiG
              9w0BAQEFAAOCAY8AMIIBigKCAYEAsOSblCwOSdS4dS/PJ9qW6HdJG1wKw0GxOlX1
              BPelDyO+7d0AeO3OlqzT0cf+kOC7UfdMtGRWa3miOafxqx+eerdhy8vec67gwT4f
              GYCoSvHq49aQsgG2/U8lG8YKcWwFYGuB5gFyMqdRpHvfzH9cxuPkopqE47i5pCg0
              TFfE7xQEzLY+UHPEd5l7i03Gml6IJ3m1pAHE/RHZOLKj3UxRUk8pey9SAR5x76wd
              3tznK93gW1K7W92JtyzNYSwrXZMwKyUwupWqwMb0fqzgv9VjwjTMxuHXor0zCpIv
              fmgjuv5FiPgCOx+0lderwRR625QbfMJm5Hb01PlfaGFZk7iNvtu+0itWUoPtzcNJ
              ZH2JYc5k/8UcoLgZ61Bjh18ddgIYj4vnmOSclVH6VEC1HB63u2NveIkfn2DMozyQ
              42KzxtZHreDnS7AnrcDFSXQ9GY5hrS38H4+hT0cDe1OHFmIyXk/jJKyDOpBSBra1
              VGlYkG4PBs0BIjitYX2ehuIkolt9AgMBAAGjRTBDMA4GA1UdDwEB/wQEAwICBDAS
              BgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQWBBRCC87b32AICaFDJq6+7WseMj6a
              tjANBgkqhkiG9w0BAQsFAAOCAYEADHWQEeIwLIhO3w4zvPN70jWt/ufpB0vtRzk+
              6u276QxHzm6yO+OrIpHWUbXOQ7sR3FWYJqpP3i4giBtKiHi8eK3FEcuB6zYx7JgP
              SD3b8SXeo+9Jz9sv62rY8pJ6s21nx1gYqqCsCDVtpiwKBAAy43/8hr/7Rg5fXAtM
              0F5tHR6vcScOgBL8ojEhhXzFUar4AVNmJmtmQFI7UEeYeT9FP0Kc1PtQNAETZuJu
              hiXjF5Y1TRS0NjPQxC0yPoHnGn94N68+m5yvSc5tUjH2oyvPCzB0tj3cE3B6hqaI
              CYBJdTTzIQjTLMurIRfjz361/6qAgZVe8VXNPdBBPpNN/20AmuNO463BuvJvRxzZ
              BGJDR312vEyYZTWSroFQNlwoPiwC7JZ2BACA90qckQy31gi+iLlAuyNpiTrbVOFZ
              n3f6VK1qc+2oqupQ6aB3BaqqgZFJEgLFMq91zD6AGp2Mg+/xr+RRs8ItOnCPb0Fb
              K2pldgj2VcEwaLFhtfywosbOzIml
              -----END CERTIFICATE-----
              -----BEGIN CERTIFICATE-----
              MIICMTCCAdigAwIBAgIQVImsiFfgWr8SMnyZPxfZdzAKBggqhkjOPQQDAjB5MR4w
              HAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJzAlBgNVBAsMHmFub25AMGMz
              YmI4YTVkOGZmIChMaW51eCBVc2VyKTEuMCwGA1UEAwwlbWtjZXJ0IGFub25AMGMz
              YmI4YTVkOGZmIChMaW51eCBVc2VyKTAeFw0yMjExMDMwMDM2MjVaFw0yNTAyMDMw
              MDM2MjVaMFIxJzAlBgNVBAoTHm1rY2VydCBkZXZlbG9wbWVudCBjZXJ0aWZpY2F0
              ZTEnMCUGA1UECwweYW5vbkAwYzNiYjhhNWQ4ZmYgKExpbnV4IFVzZXIpMFkwEwYH
              KoZIzj0CAQYIKoZIzj0DAQcDQgAE/h8ETTXtHFpwa1fp3+7PuILgHjoxYALEHBT1
              AjvbLpZCJy2IIp7ZwEvUHgFVGmpk0DKrEmMKjM+a2p5Wt8BkxKNpMGcwDgYDVR0P
              AQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMB8GA1UdIwQYMBaAFP5qbFWX
              UKQconc8vEmhoXuCD2z+MB8GA1UdEQQYMBaCFCouZ2l0bGFiLmV4YW1wbGUuY29t
              MAoGCCqGSM49BAMCA0cAMEQCIBkBUoO2mjvxOIb6oHH+jFHHzLjKlN2AOkD2obZF
              bzY/AiBhgwVXnhYJlNQWV3GcmhKR6armhCbt612h5RvyqoI30g==
              -----END CERTIFICATE-----
              EOF
              certs: [
                <<~EOF,
                -----BEGIN CERTIFICATE-----
                MIIEwTCCAymgAwIBAgIQEBuwEU7c3q23pGqWcDtSITANBgkqhkiG9w0BAQsFADB5
                MR4wHAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJzAlBgNVBAsMHmFub25A
                MmNhYTVkMTRjZWEyIChMaW51eCBVc2VyKTEuMCwGA1UEAwwlbWtjZXJ0IGFub25A
                MmNhYTVkMTRjZWEyIChMaW51eCBVc2VyKTAeFw0yMjExMDgxOTM1NDVaFw0zMjEx
                MDgxOTM1NDVaMHkxHjAcBgNVBAoTFW1rY2VydCBkZXZlbG9wbWVudCBDQTEnMCUG
                A1UECwweYW5vbkAyY2FhNWQxNGNlYTIgKExpbnV4IFVzZXIpMS4wLAYDVQQDDCVt
                a2NlcnQgYW5vbkAyY2FhNWQxNGNlYTIgKExpbnV4IFVzZXIpMIIBojANBgkqhkiG
                9w0BAQEFAAOCAY8AMIIBigKCAYEAsOSblCwOSdS4dS/PJ9qW6HdJG1wKw0GxOlX1
                BPelDyO+7d0AeO3OlqzT0cf+kOC7UfdMtGRWa3miOafxqx+eerdhy8vec67gwT4f
                GYCoSvHq49aQsgG2/U8lG8YKcWwFYGuB5gFyMqdRpHvfzH9cxuPkopqE47i5pCg0
                TFfE7xQEzLY+UHPEd5l7i03Gml6IJ3m1pAHE/RHZOLKj3UxRUk8pey9SAR5x76wd
                3tznK93gW1K7W92JtyzNYSwrXZMwKyUwupWqwMb0fqzgv9VjwjTMxuHXor0zCpIv
                fmgjuv5FiPgCOx+0lderwRR625QbfMJm5Hb01PlfaGFZk7iNvtu+0itWUoPtzcNJ
                ZH2JYc5k/8UcoLgZ61Bjh18ddgIYj4vnmOSclVH6VEC1HB63u2NveIkfn2DMozyQ
                42KzxtZHreDnS7AnrcDFSXQ9GY5hrS38H4+hT0cDe1OHFmIyXk/jJKyDOpBSBra1
                VGlYkG4PBs0BIjitYX2ehuIkolt9AgMBAAGjRTBDMA4GA1UdDwEB/wQEAwICBDAS
                BgNVHRMBAf8ECDAGAQH/AgEAMB0GA1UdDgQWBBRCC87b32AICaFDJq6+7WseMj6a
                tjANBgkqhkiG9w0BAQsFAAOCAYEADHWQEeIwLIhO3w4zvPN70jWt/ufpB0vtRzk+
                6u276QxHzm6yO+OrIpHWUbXOQ7sR3FWYJqpP3i4giBtKiHi8eK3FEcuB6zYx7JgP
                SD3b8SXeo+9Jz9sv62rY8pJ6s21nx1gYqqCsCDVtpiwKBAAy43/8hr/7Rg5fXAtM
                0F5tHR6vcScOgBL8ojEhhXzFUar4AVNmJmtmQFI7UEeYeT9FP0Kc1PtQNAETZuJu
                hiXjF5Y1TRS0NjPQxC0yPoHnGn94N68+m5yvSc5tUjH2oyvPCzB0tj3cE3B6hqaI
                CYBJdTTzIQjTLMurIRfjz361/6qAgZVe8VXNPdBBPpNN/20AmuNO463BuvJvRxzZ
                BGJDR312vEyYZTWSroFQNlwoPiwC7JZ2BACA90qckQy31gi+iLlAuyNpiTrbVOFZ
                n3f6VK1qc+2oqupQ6aB3BaqqgZFJEgLFMq91zD6AGp2Mg+/xr+RRs8ItOnCPb0Fb
                K2pldgj2VcEwaLFhtfywosbOzIml
                -----END CERTIFICATE-----
                EOF
                <<~EOF,
                -----BEGIN CERTIFICATE-----
                MIICMTCCAdigAwIBAgIQVImsiFfgWr8SMnyZPxfZdzAKBggqhkjOPQQDAjB5MR4w
                HAYDVQQKExVta2NlcnQgZGV2ZWxvcG1lbnQgQ0ExJzAlBgNVBAsMHmFub25AMGMz
                YmI4YTVkOGZmIChMaW51eCBVc2VyKTEuMCwGA1UEAwwlbWtjZXJ0IGFub25AMGMz
                YmI4YTVkOGZmIChMaW51eCBVc2VyKTAeFw0yMjExMDMwMDM2MjVaFw0yNTAyMDMw
                MDM2MjVaMFIxJzAlBgNVBAoTHm1rY2VydCBkZXZlbG9wbWVudCBjZXJ0aWZpY2F0
                ZTEnMCUGA1UECwweYW5vbkAwYzNiYjhhNWQ4ZmYgKExpbnV4IFVzZXIpMFkwEwYH
                KoZIzj0CAQYIKoZIzj0DAQcDQgAE/h8ETTXtHFpwa1fp3+7PuILgHjoxYALEHBT1
                AjvbLpZCJy2IIp7ZwEvUHgFVGmpk0DKrEmMKjM+a2p5Wt8BkxKNpMGcwDgYDVR0P
                AQH/BAQDAgWgMBMGA1UdJQQMMAoGCCsGAQUFBwMBMB8GA1UdIwQYMBaAFP5qbFWX
                UKQconc8vEmhoXuCD2z+MB8GA1UdEQQYMBaCFCouZ2l0bGFiLmV4YW1wbGUuY29t
                MAoGCCqGSM49BAMCA0cAMEQCIBkBUoO2mjvxOIb6oHH+jFHHzLjKlN2AOkD2obZF
                bzY/AiBhgwVXnhYJlNQWV3GcmhKR6armhCbt612h5RvyqoI30g==
                -----END CERTIFICATE-----
                EOF
              ],
            },
          ].freeze

          tests.each do |t|
            context "#{t[:name]}" do
              let(:variables) { { ADDITIONAL_CA_CERT_BUNDLE: t[:input] } }

              let(:script) do
                <<~EOF
                #!/bin/bash
                set -euo pipefail

                /analyzer run

                for idx in {0..#{t[:certs].length-1}}; do
                  /opt/asdf/shims/keytool -list -cacerts -rfc -noprompt -storepass changeit -alias gl-dependency-scanning-custom-cert-$idx
                done
                EOF
              end


              it "successfully adds the cert bundle to the keystore" do
                # The keytool uses CRLF line endings instead of LF which causes
                # The comparison to fail unless the line endings are replaced.
                output = scan.combined_output.gsub("\r", "")
                t[:certs].each { |cert| expect(output).to include(cert) }
              end

              describe "CycloneDX SBOMs" do
                let(:relative_sbom_paths) { ["gl-sbom-maven-gradle.cdx.json"] }

                it_behaves_like "non-empty CycloneDX files"
                it_behaves_like "recorded CycloneDX files"
                it_behaves_like "valid CycloneDX files"
              end
            end
          end
        end
      end

      context "with build using implementation directive and nested dependencies", run_parallel: true do
        let(:project) { "java-gradle/nested-dependencies-with-implementation-directive" }
        let(:expected_report_dir) { "java-gradle/nested-dependencies-with-implementation-directive" }
        let(:variables) { { "GEMNASIUM_DB_REF_NAME": "v2.0.749" } }

        it_behaves_like 'a complete scan'
      end
    end

    context 'using scala and sbt' do
      let(:project) { "scala-sbt/default" }
      let(:expected_report_dir) { "scala-sbt/default" }

      context 'default version of java' do
        context 'sbt 1.3', run_parallel: true do
          let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.3.12" } }

          it_behaves_like 'a complete scan'

          describe "CycloneDX SBOMs" do
            let(:relative_sbom_paths) { ["gl-sbom-maven-sbt.cdx.json"] }

            it_behaves_like "non-empty CycloneDX files"
            it_behaves_like "recorded CycloneDX files"
            it_behaves_like "valid CycloneDX files"
          end
        end

        context 'sbt 1.4', run_parallel: true do
          let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.4.6" } }

          it_behaves_like 'a complete scan'
        end

        context 'sbt 1.5', run_parallel: true do
          let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.5.8" } }

          it_behaves_like 'a complete scan'
        end

        context 'sbt 1.6', run_parallel: true do
          let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.6.2" } }

          it_behaves_like 'a complete scan'

          context 'when not using addDependencyTreePlugin in plugins.sbt' do
            let(:project) { "scala-sbt/sbt-dependency-graph-bug" }

            # TODO check that scan is successful when https://gitlab.com/gitlab-org/gitlab/-/issues/390287 is fixed
            it 'raises an exception', :aggregate_failures do
              expect(scan.combined_output).to include("java.util.NoSuchElementException: key not found: GraphModuleId(org.apache.commons,commons-io,1.3.2")
              expect(scan.exit_code).to_not be_zero
            end
          end
        end

        context 'sbt 1.7', run_parallel: true do
          let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.7.3" } }

          it_behaves_like 'a complete scan'
        end

        context 'sbt 1.8', run_parallel: true do
          let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.8.3" } }

          it_behaves_like 'a complete scan'
        end

        context 'sbt 1.9', run_parallel: true do
          let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.9.6" } }

          it_behaves_like 'a complete scan'
        end
      end

      context 'java 8', run_parallel: true do
        let(:variables) { { "DS_JAVA_VERSION": 8 } }

        it_behaves_like 'a complete scan'
      end

      context 'java 11' do
        let(:variables) { { "DS_JAVA_VERSION": 11 } }

        it_behaves_like 'a complete scan'

        context 'sbt 1.1', run_parallel: true do
          let(:variables) { super().merge("SBT_CLI_OPTS": "-Dsbt.version=1.1.6") }

          it_behaves_like 'a complete scan'
        end

        context 'sbt 1.2', run_parallel: true do
          let(:variables) { super().merge("SBT_CLI_OPTS": "-Dsbt.version=1.2.8") }

          it_behaves_like 'a complete scan'
        end
      end

      context 'java 21', run_parallel: true, build: 'debian' do
        let(:variables) { { "SBT_CLI_OPTS": "-Dsbt.version=1.9.6", "DS_JAVA_VERSION": 21 } }

        it_behaves_like 'a complete scan'
      end

      context 'multiproject', run_parallel: true  do
        let(:variables) { { "DS_JAVA_VERSION": "11" } }
        let(:project) { "scala-sbt/multiproject/default" }
        let(:expected_report_dir) { "scala-sbt/multiproject/default" }

        it_behaves_like 'a complete scan'

        describe "SBOM Manifest" do
          let(:relative_sbom_manifest_path) { "sbom-manifest.json" }

          it_behaves_like "non-empty SBOM manifest"
          it_behaves_like "recorded SBOM manifest"
        end

        describe "CycloneDX SBOMs" do
          let(:relative_sbom_paths) { ["gl-sbom-maven-sbt.cdx.json", "proj1/gl-sbom-maven-sbt.cdx.json", "proj2/gl-sbom-maven-sbt.cdx.json"] }

          it_behaves_like "non-empty CycloneDX files"
          it_behaves_like "recorded CycloneDX files"
          it_behaves_like "valid CycloneDX files"
        end
      end
    end
  end
end
