package manifest

import (
	"fmt"
	"time"
)

// Time is used for storing time related data for the manifest
type Time struct {
	time.Time
}

// MarshalJSON is responsible for converting the native timestamp value in the manifest to
// a formatted string timestamp
func (t Time) MarshalJSON() ([]byte, error) {
	layoutISO := "2006-01-02T15:04:05Z"
	timestamp := fmt.Sprintf("\"%s\"", t.Format(layoutISO))

	return []byte(timestamp), nil
}

// Manifest contains the complete manifest/index
type Manifest struct {
	// Version of the Manifest file format
	Version    string      `json:"version,omitempty"`
	Analyzer   Analyzer    `json:"analyzer,omitempty"`
	Components []Component `json:"components,omitempty"`
	Timestamp  Time        `json:"timestamp,omitempty"`
}

// Analyzer provides details about the analyzer used for generating this manifest
type Analyzer struct {
	ID      string `json:"id,omitempty"`
	Version string `json:"version,omitempty"`
}

// Component represents all the data related to an SBOM
type Component struct {
	Project        Project `json:"project,omitempty"`
	PackageType    string  `json:"package_type,omitempty"`
	PackageManager string  `json:"package_manager,omitempty"`
	Language       string  `json:"language,omitempty"`
	Files          []File  `json:"files,omitempty"`
}

// Project is where the build file was detected that generated the SBOM
type Project struct {
	Path string `json:"path,omitempty"`
}

// File is a reference to a file in the project
type File struct {
	Type string `json:"type,omitempty"`
	Path string `json:"path,omitempty"`
}
