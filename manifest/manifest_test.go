package manifest_test

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"regexp"
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/cyclonedx"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/manifest"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func TestManifest(t *testing.T) {
	t.Run("Create", func(t *testing.T) {
		sboms := []cyclonedx.SBOM{
			{
				InputFilePath:  "z-project/Gemfile.lock",
				OutputFilePath: "z-project/gl-sbom-gem-bundler.cdx.json",
				PackageManager: "bundler",
				PackageType:    "a-sorted-second-last",
			},
			{
				InputFilePath:  "z-project/go.sum",
				OutputFilePath: "z-project/gl-sbom-go-go.cdx.json",
				PackageManager: "go",
				PackageType:    "z-sorted-last",
			},
			{
				InputFilePath:  "Gemfile.lock",
				OutputFilePath: "gl-sbom-gem-bundler.cdx.json",
				PackageManager: "bundler",
				PackageType:    "gem",
			},
			{
				InputFilePath:  "javascript-project/yarn.lock",
				OutputFilePath: "javascript-project/gl-sbom-npm-yarn.cdx.json",
				PackageManager: "yarn",
				PackageType:    "npm",
			},
		}

		expectedJSON := `{
      "version": "0.0.1",
      "components": [
        {
          "project": {
            "path": "."
          },
          "package_type": "gem",
          "package_manager": "bundler",
          "language": "Ruby",
          "files": [
            {
              "type": "sbom",
              "path": "gl-sbom-gem-bundler.cdx.json"
            },
            {
              "type": "input",
              "path": "Gemfile.lock"
            }
          ]
        },
        {
          "project": {
            "path": "javascript-project"
          },
          "package_type": "npm",
          "package_manager": "yarn",
          "language": "JavaScript",
          "files": [
            {
              "type": "sbom",
              "path": "javascript-project/gl-sbom-npm-yarn.cdx.json"
            },
            {
              "type": "input",
              "path": "javascript-project/yarn.lock"
            }
          ]
        },
        {
          "project": {
            "path": "z-project"
          },
          "package_type": "a-sorted-second-last",
          "package_manager": "bundler",
          "language": "Ruby",
          "files": [
            {
              "type": "sbom",
              "path": "z-project/gl-sbom-gem-bundler.cdx.json"
            },
            {
              "type": "input",
              "path": "z-project/Gemfile.lock"
            }
          ]
        },
        {
          "project": {
            "path": "z-project"
          },
          "package_type": "z-sorted-last",
          "package_manager": "go",
          "language": "Go",
          "files": [
            {
              "type": "sbom",
              "path": "z-project/gl-sbom-go-go.cdx.json"
            },
            {
              "type": "input",
              "path": "z-project/go.sum"
            }
          ]
        }
      ],
      "timestamp": "2022-04-08T14:52:37Z",
      "analyzer": {
        "id": "gemnasium",
        "version": "2.36.0"
      }
    }`

		tmpDir, err := ioutil.TempDir("", "")
		require.NoError(t, err)
		defer os.RemoveAll(tmpDir)

		err = manifest.Create(tmpDir, sboms, report.ScannerDetails{ID: "gemnasium", Version: "2.36.0"})
		require.NoError(t, err)

		actualJSON, err := ioutil.ReadFile(filepath.Join(tmpDir, "sbom-manifest.json"))
		require.NoError(t, err)
		require.JSONEq(t, sanitize(expectedJSON), sanitize(string(actualJSON)))
	})
}

func sanitize(json string) string {
	re := regexp.MustCompile(`"timestamp": ".*"`)
	return re.ReplaceAllString(json, `"timestamp": "removed"`)
}
