package scanner

import (
	"fmt"
	"os"
	"path/filepath"

	log "github.com/sirupsen/logrus"
	"github.com/urfave/cli/v2"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/advisory"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/finder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/libfinder"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/vrange"
)

const (
	flagPrefix           = "gemnasium-"
	flagDBUpdateDisabled = flagPrefix + "db-update-disabled"
	flagDBLocalPath      = flagPrefix + "db-local-path"
	flagDBRemoteURL      = flagPrefix + "db-remote-url"
	flagDBWebURL         = flagPrefix + "db-web-url"
	flagDBRefName        = flagPrefix + "db-ref-name"

	flagIncludeDevDependencies = "include-dev-deps"

	envVarPrefix           = "GEMNASIUM_"
	envVarDBUpdateDisabled = envVarPrefix + "DB_UPDATE_DISABLED"
	envVarDBLocalPath      = envVarPrefix + "DB_LOCAL_PATH"
	envVarDBRemoteURL      = envVarPrefix + "DB_REMOTE_URL"
	envVarDBWebURL         = envVarPrefix + "DB_WEB_URL"
	envVarDBRefName        = envVarPrefix + "DB_REF_NAME"

	envVarIncludeDevDependencies = "DS_INCLUDE_DEV_DEPENDENCIES"
)

// Flags generates new command line flags for the scanner
func Flags() []cli.Flag {
	return []cli.Flag{
		&cli.BoolFlag{
			Name:    flagDBUpdateDisabled,
			EnvVars: []string{envVarDBUpdateDisabled},
			Usage:   "Disable gemnasium-db git repo update before scanning",
			Value:   false,
		},
		&cli.StringFlag{
			Name:    flagDBLocalPath,
			EnvVars: []string{envVarDBLocalPath},
			Usage:   "Path of gemnasium-db git repo",
			Value:   "gemnasium-db",
		},
		&cli.StringFlag{
			Name:    flagDBRemoteURL,
			EnvVars: []string{envVarDBRemoteURL},
			Usage:   "Remote URL the local gemnasium-db git repo is synced with",
		},
		&cli.StringFlag{
			Name:    flagDBWebURL,
			EnvVars: []string{envVarDBWebURL},
			Usage:   "Web URL of the gemnasium-db GitLab project",
		},
		&cli.StringFlag{
			Name:    flagDBRefName,
			EnvVars: []string{envVarDBRefName},
			Usage:   "git reference the local gemnasium-db git repo is synced with",
		},
		&cli.BoolFlag{
			Name:    flagIncludeDevDependencies,
			EnvVars: []string{envVarIncludeDevDependencies},
			Usage:   "Include development dependencies in scan",
			Value:   true,
		},
	}
}

// NewScanner parses command line arguments from a cli.Context and returns a new scanner
func NewScanner(c *cli.Context) (*Scanner, error) {
	// GitLab project and git repo for the advisories
	r := advisory.Repo{
		Path:      c.String(flagDBLocalPath),
		RemoteURL: c.String(flagDBRemoteURL),
		WebURL:    c.String(flagDBWebURL),
		RefName:   c.String(flagDBRefName),
	}

	parserOpts := parser.Options{
		IncludeDev: c.Bool(flagIncludeDevDependencies),
	}
	// update advisory repo if requested
	if !c.Bool(flagDBUpdateDisabled) {
		if err := r.Update(); err != nil {
			return nil, err
		}
	}

	return &Scanner{r, parserOpts}, nil
}

// Scanner is a project scanner
type Scanner struct {
	repo       advisory.Repo
	parserOpts parser.Options
}

// ScanProjects scans projects and returns a description of each file it has scanned.
// This includes the dependencies found in the file, and the vulnerabilities affecting them.
func (s Scanner) ScanProjects(dir string, projects []finder.Project) ([]File, error) {
	result := []File{}
	for _, project := range projects {
		scannable, isScannable := project.ScannableFile()
		if !isScannable {
			// skip project
			log.Debugf("skip project: no scannable file found in %s", project.Dir)
			continue
		}

		// relative and absolute path of scannable file
		rel := project.FilePath(scannable)
		path := filepath.Join(dir, rel)

		// set location to relative path of scannable file
		location := rel
		if !scannable.Linkable() {
			// update location with the relative path of the requirements file
			if requirements, ok := project.RequirementsFile(); ok {
				// NOTE this code makes the assumption that the requirements file will
				// be in the same directory as the export file but this isn't always the
				// case e.g. in the case of Gradle's htmlDependencyReport.
				location = project.FilePath(requirements)
				log.Debugf("Location set to %s", location)
			}
		}

		// scan dependency file
		file := File{
			RootDir:        dir,
			Path:           location,
			PackageManager: project.PackageManager.Name,
		}
		if err := s.scanFile(path, &file); err != nil {
			return nil, fmt.Errorf("scanning file %s: %v", path, err)
		}
		result = append(result, file)
	}

	return result, nil
}

// ScanLibs scans each vendored library and returns files
// that combine a file path with vulnerabilities.
func (s Scanner) ScanLibs(dir string, libs []libfinder.Library) ([]File, error) {
	result := []File{}
	for _, lib := range libs {
		pkgs := []parser.Package{lib.Package}
		pkgType := string(lib.PackageType)

		// convert to scanner file
		file := File{
			RootDir:     dir,
			Path:        lib.Path,
			PackageType: pkgType,
			Packages:    pkgs,
		}

		// find affections
		aff, err := pkgAffections(pkgs, pkgType, s.repo)
		if err != nil {
			return nil, err
		}
		file.Affections = aff

		result = append(result, file)
	}

	return result, nil
}

// scanFile opens, parses, and scans a dependency file with a given path.
// It updates the packages, dependencies, package type, and affections
// of the given File struct.
func (s Scanner) scanFile(path string, file *File) error {
	// open file
	f, err := os.Open(path)
	if err != nil {
		return fmt.Errorf("opening file %s: %v", path, err)
	}
	defer f.Close()

	// find file parser
	basename := filepath.Base(path)
	depParser := parser.Lookup(basename)
	if depParser == nil {
		return fmt.Errorf("no file parser for: %s", path)
	}
	pkgType := string(depParser.PackageType)
	file.PackageType = pkgType

	pkgs, deps, err := depParser.Parse(f, s.parserOpts)
	if err != nil {
		return fmt.Errorf("parsing file %s: %v", path, err)
	}
	file.Packages = pkgs
	file.Dependencies = deps

	// find affections
	aff, err := pkgAffections(pkgs, pkgType, s.repo)
	if err != nil {
		return fmt.Errorf("finding package affections for file %s: %w", path, err)
	}
	file.Affections = aff

	return nil
}

type queryTranslator func(vrange.Query, []advisory.Version) vrange.Query

// pkgAffections returns affections for packages of a given type.
//
// For each package, this functions finds the security advisories matching the package type and name,
// then filters out the security advisories that don't match the package version.
// The function returns affections;
// an affection combines a package with a security advisory affecting that package.
func pkgAffections(pkgs []parser.Package, pkgType string, repo advisory.Repo) ([]Affection, error) {
	// version range resolver
	resolver, err := vrange.NewResolver(pkgTypeToResolverName(pkgType))
	if err != nil {
		return nil, fmt.Errorf("creating resolver pkgType=%s: %v", pkgType, err)
	}

	// query translator
	var translateQuery queryTranslator
	switch t := resolver.(type) {
	default:
		// use identify function
		translateQuery = func(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
			return q
		}
	case vrange.QueryTranslator:
		// delegate to resolver
		translateQuery = func(q vrange.Query, versionMeta []advisory.Version) vrange.Query {
			return t.TranslateQuery(q, versionMeta)
		}
	}

	// ensure repo has advisories matching the package type
	if err := repo.SatisfyPackageType(pkgType); err != nil {
		return nil, fmt.Errorf("satisfying package type advisories: %v", err)
	}

	// collect possible affections, list version range queries
	affections := []Affection{}
	queries := []vrange.Query{}
	for _, pkg := range pkgs {
		// list package advisories
		apkg := advisory.Package{Type: pkgType, Name: pkg.Name}
		paths, err := repo.PackageAdvisories(apkg)
		if _, ok := err.(advisory.ErrNoPackageDir); ok {
			continue // no advisories
		}
		if err != nil {
			return nil, fmt.Errorf("fetching package advisories: %v", err)
		}

		// fetch advisories, append affections and queries
		for _, path := range paths {
			adv, err := repo.Advisory(path)
			if err != nil {
				return nil, fmt.Errorf("fetching advisories: %v", err)
			}
			aff := Affection{pkg, *adv}
			affections = append(affections, aff)
			q := translateQuery(aff.query(), aff.Advisory.Versions)
			queries = append(queries, q)

		}
	}

	// resolve queries and tell if dependency version is in affected range
	result, err := resolver.Resolve(queries)
	if err != nil {
		return nil, fmt.Errorf("resolving queries: %v", err)
	}

	// filter affections and keep those where the dependency version is in affected range
	filtered := []Affection{}
	for _, aff := range affections {
		q := translateQuery(aff.query(), aff.Advisory.Versions)
		ok, err := result.Satisfies(q)
		if err != nil {
			// TODO report error
			continue
		}
		if ok {
			filtered = append(filtered, aff)
		}
	}
	return filtered, nil
}

// pkgTypeToResolverName converts a package type to the name of a vrange resolver.
func pkgTypeToResolverName(pkgType string) string {
	switch pkgType {
	case "gradle":
		return "maven"
	case "npm", "maven", "gem", "go":
		return pkgType
	case "packagist":
		return "php"
	case "pypi":
		return "python"
	default:
		return pkgType
	}
}
