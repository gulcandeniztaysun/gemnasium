package yarn

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser/testutil"
)

func TestYarnParse(t *testing.T) {
	for _, tc := range []string{"classic/simple", "classic/big", "berry/v2/simple", "berry/v2/big", "berry/v3", "berry/v4"} {
		t.Run(tc, func(t *testing.T) {
			fixture := testutil.Fixture(t, tc, "yarn.lock")
			pkgs, deps, err := Parse(fixture, parser.Options{})
			require.NoError(t, err)

			t.Run("packages", func(t *testing.T) {
				testutil.RequireExpectedPackages(t, tc, pkgs)
			})

			t.Run("dependencies", func(t *testing.T) {
				testutil.RequireExpectedDependencies(t, tc, deps)
			})
		})
	}

	t.Run("Yarn parse failure cases", func(t *testing.T) {
		tcs := []struct {
			name string
			path string
			want string
		}{
			{
				"Yarn classic lock file with wrong dependency path",
				"classic/malformed/wrong-dependency-path",
				`cannot find yarn dependency mime-types@~32.1.11 required by accepts@1.3.3`,
			},
			{
				"Yarn classic lock file with wrong definiton",
				"classic/malformed/wrong-definition",
				`invalid spec: wrong spec`,
			},
			{
				"Yarn classic lock file with wrong dependency structure",
				"classic/malformed/wrong-dependency",
				`cannot parse dependency: mime-types "~2.1.11"`,
			},
			{
				"Yarn classic lock file with wrong version",
				"classic/malformed/wrong-version",
				"invalid yarn lockfile version: 5",
			},
			{
				"Yarn berry lock file with metadata version above yarn v4",
				"berry/malformed/wrong-version",
				"unsupported yarn.lock file version 30",
			},
			{
				"Yarn berry lock file cannot be parsed as yaml",
				"berry/malformed/cannot-be-parsed",
				"failed to parse yarn.lock as a yaml file: yaml: line 17: could not find expected ':'",
			},
		}

		for _, tc := range tcs {
			t.Run(tc.name, func(t *testing.T) {
				fixture := testutil.Fixture(t, tc.path, "yarn.lock")
				pkgs, deps, err := Parse(fixture, parser.Options{})
				require.Empty(t, pkgs)
				require.Empty(t, deps)
				require.EqualError(t, err, tc.want)
			})
		}
	})
}
