package pnpm

import (
	"fmt"
	"regexp"

	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

// Lockfile is a pnpm lock file
type Lockfile struct {
	Version  string             `yaml:"lockfileVersion"`
	Packages map[string]Package `yaml:"packages"`
	parser.Options
}

// Package contains the details for a package entry in the packages section
type Package struct {
	// Name/Version are only present for packages that aren't in npm, for example, a tarball
	Name    string `yaml:"name"`
	Version string `yaml:"version"`
	Dev     bool   `yaml:"dev"`
}

var v5VersionRegex = regexp.MustCompile(`^5\.\d+$`)
var v6VersionRegex = regexp.MustCompile(`^6\.\d+$`)

// Returns whether the given lockfile version can be parsed
func isSupportedVersion(version string) error {
	supportedVersionRegexes := []*regexp.Regexp{v5VersionRegex, v6VersionRegex}

	for _, supportedVersionRegex := range supportedVersionRegexes {
		if supportedVersionRegex.MatchString(version) {
			return nil
		}
	}

	return parser.ErrWrongFileFormatVersion
}

/*
version 5.x uses the following dependency path format:

/<package-name>/<package-version>_<parent-package>@<parent-version>

The parent-package@version after the underscore at the end is optional.

Examples:

	/@remix-run/router/1.1.0
	/@babel/helper-compilation-targets/7.20.7_@babel+core@7.20.7
	/utils-merge/1.0.1
*/
var v5DepPathRegex = regexp.MustCompile(`^/(?P<Name>.*)/(?P<Version>[^_]*)`)

/*
version 6.x uses the following dependency path format:

/<package-name>@<package-version>(<parent-package>@<parent-version>)

The parent-package@version in parentheses at the end is optional.

Examples:

	/request-promise-native@1.0.9(request@2.88.2)
	/acorn-globals@4.3.4
	/@adobe/css-tools@4.2.0
*/

var v6DepPathRegex = regexp.MustCompile(`^/(?P<Name>@?.*?)@(?P<Version>[^(]*)`)

func (f Lockfile) extractPackageNameAndVersion(dependencyPath string) (string, string, error) {
	re := v5DepPathRegex
	parseErr := fmt.Errorf("unable to parse dependencyPath %q", dependencyPath)

	if v6VersionRegex.MatchString(f.Version) {
		re = v6DepPathRegex
	}

	matches := re.FindStringSubmatch(dependencyPath)

	if len(matches) == 0 {
		return "", "", parseErr
	}

	name := matches[re.SubexpIndex("Name")]
	version := matches[re.SubexpIndex("Version")]

	if name == "" || version == "" {
		return "", "", parseErr
	}

	return name, version, nil
}

// Parse returns packages without duplicates
func (f Lockfile) Parse() ([]parser.Package, error) {
	if err := isSupportedVersion(f.Version); err != nil {
		return nil, err
	}
	pkgs := make([]parser.Package, 0, len(f.Packages))

	for dependencyPath, pkg := range f.Packages {
		// skip package if the option to include devDependencies has been set to false.
		if pkg.Dev && !f.IncludeDev {
			continue
		}

		// pnpm lockfiles for tarballs contain Name and Version fields in the package details,
		// so we attempt to use those first, if available.
		name := pkg.Name
		version := pkg.Version

		// If the package Name or Version is unknown, attempt to obtain them by parsing the dependencyPath
		if name == "" || version == "" {
			var err error
			name, version, err = f.extractPackageNameAndVersion(dependencyPath)
			if err != nil {
				log.Warnf("pnpm parser: %s. Skipping dependency.", err)
				continue
			}
		}

		pkgs = append(pkgs, parser.Package{Name: name, Version: version})
	}

	return pkgs, nil
}
