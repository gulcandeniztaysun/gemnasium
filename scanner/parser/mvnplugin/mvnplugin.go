package mvnplugin

import (
	"encoding/json"
	"io"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
)

// Dependency is used to decode a project dependency from output
type Dependency struct {
	GroupID    string `json:"groupId"`
	ArtifactID string `json:"artifactId"`
	Version    string `json:"version"` // Installed version
}

// Parse scans the output of the Gemnasium Maven plugin and returns a list of packages
func Parse(r io.Reader, opts parser.Options) ([]parser.Package, []parser.Dependency, error) {
	processed := map[string]bool{}
	deps := []Dependency{}
	err := json.NewDecoder(r).Decode(&deps)
	if err != nil {
		return nil, nil, err
	}
	results := []parser.Package{}
	for _, dep := range deps {
		name := dep.GroupID + "/" + dep.ArtifactID
		if processed[name] {
			continue
		}
		results = append(results, parser.Package{Name: name, Version: dep.Version})
		processed[name] = true
	}
	return results, nil, nil
}

func init() {
	parser.Register("mvnplugin", parser.Parser{
		Parse:       Parse,
		PackageType: parser.PackageTypeMaven,
		Filenames:   []string{"maven-dependencies.json", "gemnasium-maven-plugin.json", "gradle-dependencies.json"},
	})
}
