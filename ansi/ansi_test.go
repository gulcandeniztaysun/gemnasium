package ansi_test

import (
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/ansi"
)

func TestStripAll(t *testing.T) {
	tests := map[string]struct {
		input  string
		expect string
	}{
		"gradle console output with color": {
			input: `
[1A[1m<[0;1m-------------> 0% INITIALIZING [59ms][m[38D[1B
[2A[1m<[0;1m-------------> 0% INITIALIZING [159ms][m[39D[1B[1m> Running init scripts[m[22D[1B[2A[1m<[0;1m-------------> 0% CONFIGURING [259ms][m[0K[38D[1B[1m> root project[m[0K[14D[1B[2A[1m<[0;32;1m=============[0;39;1m> 100% CONFIGURING [359ms][m[40D[1B> IDLE[0K[6D[1B[2A[1m<[0;1m-------------> 0% EXECUTING [459ms][m[0K[36D[1B[1m> :gemnasiumDumpDependencies > Resolve dependencies of :compileClasspath[m[72D[1B[2A[1m<[0;1m-------------> 0% EXECUTING [559ms][m[36D[2B[2A[1m<[0;1m-------------> 0% EXECUTING [659ms][m[36D[1B[1m> :gemnasiumDumpDependencies[m[0K[28D[1B[2A[1m<[0;1m-------------> 0% EXECUTING [759ms][m[36D[2B[2A[1m<[0;1m-------------> 0% EXECUTING [860ms][m[36D[2B[2A[0K
[1m> Task :gemnasiumDumpDependencies[m[0K
Writing dependency JSON to /app/gradle-dependencies.json

Deprecated Gradle features were used in this build, making it incompatible with Gradle 8.0.

You can use '--warning-mode all' to show the individual deprecation warnings and determine if they come from your own scripts or plugins.

See https://docs.gradle.org/7.3.3/userguide/command_line_interface.html#sec:command_line_warnings

[32;1mBUILD SUCCESSFUL[0;39m in 4s
1 actionable task: 1 executed
[0K
[0K
[2A[1m<[0;1m-------------> 0% WAITING[m[26D[1B[1m> :gemnasiumDumpDependencies[m[28D[1B[2A[2K[1B[2K[1A`,
			expect: `
<-------------> 0% INITIALIZING [59ms]
<-------------> 0% INITIALIZING [159ms]> Running init scripts<-------------> 0% CONFIGURING [259ms]> root project<=============> 100% CONFIGURING [359ms]> IDLE<-------------> 0% EXECUTING [459ms]> :gemnasiumDumpDependencies > Resolve dependencies of :compileClasspath<-------------> 0% EXECUTING [559ms]<-------------> 0% EXECUTING [659ms]> :gemnasiumDumpDependencies<-------------> 0% EXECUTING [759ms]<-------------> 0% EXECUTING [860ms]
> Task :gemnasiumDumpDependencies
Writing dependency JSON to /app/gradle-dependencies.json

Deprecated Gradle features were used in this build, making it incompatible with Gradle 8.0.

You can use '--warning-mode all' to show the individual deprecation warnings and determine if they come from your own scripts or plugins.

See https://docs.gradle.org/7.3.3/userguide/command_line_interface.html#sec:command_line_warnings

BUILD SUCCESSFUL in 4s
1 actionable task: 1 executed


<-------------> 0% WAITING> :gemnasiumDumpDependencies`,
		},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got := ansi.StripAll(tt.input)
			assert.Equal(t, tt.expect, got, "did not strip all ansi codes")
		})
	}
}
