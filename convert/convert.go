package convert

import (
	"time"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

// Converter converts files returned by the scanner
// to Dependency Scanning reports
type Converter struct {
	AnalyzerDetails report.AnalyzerDetails
	ScannerDetails  report.ScannerDetails
	StartTime       *time.Time
	SchemaModel     uint
	DepPathMode     DepPathMode
}

// ToReport converts dependency files returned by the scanner
// to a Dependency Scanning report
func (c Converter) ToReport(scanFiles []scanner.File) *report.Report {
	// collect vulnerabilities and dependency files
	vulns := []report.Vulnerability{}
	depfiles := []report.DependencyFile{}
	for _, scanFile := range scanFiles {
		// add affections as vulnerabilities
		pathMode := c.depPathModeForFile(scanFile)
		fc := NewFileConverter(scanFile, pathMode, c.SchemaModel)
		vulns = append(vulns, fc.Vulnerabilities()...)

		// add as dependency file if handled by package manager
		if scanFile.PackageManager != "" {
			depfiles = append(depfiles, fc.DependencyFile())
		}
	}

	// initialize report with vulnerabilities, dep. files, and scan object
	r := report.NewReport()
	r.Vulnerabilities = vulns
	r.DependencyFiles = depfiles
	r.Scan.Scanner = c.ScannerDetails
	r.Scan.Analyzer = c.AnalyzerDetails
	r.Scan.Type = report.CategoryDependencyScanning
	r.Scan.Status = report.StatusSuccess

	// set start time and end time if start time is configured
	if c.StartTime != nil {
		startTime := report.ScanTime(*c.StartTime)
		r.Scan.StartTime = &startTime
		endTime := report.ScanTime(time.Now())
		r.Scan.EndTime = &endTime
	}

	return &r
}

func (c Converter) depPathModeForFile(f scanner.File) DepPathMode {
	if len(f.Dependencies) == 0 {
		// dependency graph information not available
		return DepPathModeNone
	}
	return c.DepPathMode
}
