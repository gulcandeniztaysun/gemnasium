package convert

import (
	"testing"

	"github.com/stretchr/testify/require"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

func Test_DependencyFile(t *testing.T) {
	fileConverter := FileConverter{
		depConverters: []DependencyConverter{
			{
				Package: parser.Package{
					Name:    "dep1",
					Version: "1.0.0",
				},
			},
			{
				Package: parser.Package{
					Name:    ".dep2",
					Version: "1.0.1",
				},
			},
			{
				Package: parser.Package{
					Name:    ".\\dep3",
					Version: "1.0.2",
				},
			},
			{
				Package: parser.Package{
					Name:    "./dep4",
					Version: "1.0.3",
				},
			},
			{
				Package: parser.Package{
					Name: "dep with no version",
				},
			},
		},
	}

	want := []report.Dependency{
		{
			Package: report.Package{Name: "dep1"},
			Version: "1.0.0",
		},
		{
			Package: report.Package{Name: "dep2"},
			Version: "1.0.1",
		},
		{
			Package: report.Package{Name: "dep3"},
			Version: "1.0.2",
		},
		{
			Package: report.Package{Name: "dep4"},
			Version: "1.0.3",
		},
	}

	for i, dep := range fileConverter.DependencyFile().Dependencies {
		require.Equal(t, want[i], dep)
	}
}
