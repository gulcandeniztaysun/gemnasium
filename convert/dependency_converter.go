package convert

import (
	"fmt"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

// DependencyConverter converts a package and its dependency path
type DependencyConverter struct {
	Package      parser.Package
	ShortestPath []Node
	IID          uint
}

func (c DependencyConverter) dependency() report.Dependency {
	return report.Dependency{
		Package: report.Package{Name: c.Package.ResolvedName()},
		Version: c.Package.Version,
		IID:     c.IID,
	}
}

func (c DependencyConverter) dependencyWithPath() report.Dependency {
	dep := c.dependency()
	if path := c.ShortestPath; path != nil {
		switch len(path) {
		case 0:
			// make dependency as direct
			dep.Direct = true
		default:
			// convert nodes to dependency refs
			for _, node := range path {
				ref := report.DependencyRef{IID: node.IID}
				dep.DependencyPath = append(dep.DependencyPath, ref)
			}
		}
	}
	return dep
}

func (c DependencyConverter) details() *report.Details {
	details := report.Details{
		"vulnerable_package": report.DetailsTextField{
			Name:  "Vulnerable Package",
			Value: formatPkg(c.Package),
		},
	}
	if ancestors := c.ShortestPath; len(ancestors) > 0 {
		if introducer := ancestors[0].Package; introducer != nil {
			details["introduced_by_package"] = report.DetailsTextField{
				Name:  "Introduced by Package",
				Value: formatPkg(*introducer),
			}
		}
		details["shortest_path"] = DetailsTextListField{
			Name:  "Shortest Path",
			Items: shortestPathItems(ancestors),
		}
	}
	return &details
}

func formatPkg(pkg parser.Package) string {
	return fmt.Sprintf("%s:%s", pkg.ResolvedName(), pkg.Version)
}

func shortestPathItems(nodes []Node) []DetailsNamelessTextField {
	items := []DetailsNamelessTextField{}

	for _, node := range nodes {
		path := "?"
		if node.Package != nil {
			path = formatPkg(*node.Package)
		}

		item := DetailsNamelessTextField{
			Value: path,
		}

		items = append(items, item)
	}

	return items
}
