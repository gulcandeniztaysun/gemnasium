package convert

import (
	log "github.com/sirupsen/logrus"

	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner"
	"gitlab.com/gitlab-org/security-products/analyzers/gemnasium/v5/scanner/parser"
	"gitlab.com/gitlab-org/security-products/analyzers/report/v4"
)

// DepPathMode is the strategy used to enable dependency paths
type DepPathMode int

const (
	// DepPathModeNone is when dependency paths are disabled
	DepPathModeNone = DepPathMode(iota)

	// DepPathModeAffected is when dependency paths are enabled
	// only for packages that are affected
	DepPathModeAffected

	// DepPathModeAll is when dependency paths are enabled
	// for all dependencies
	DepPathModeAll
)

// setDepPathFunc is the type of a function that tells if the dependency path
// should be rendered for a given package
type setDepPathFunc func(parser.Package) bool

// NewFileConverter returns a new FileConverter
// where the package index and the dependency graph have been initialized if needed.
func NewFileConverter(f scanner.File, depPathMode DepPathMode, schemaModel uint) *FileConverter {
	var index *Index              // index used to turn packages into IIDs
	var graph *Graph              // graph used to calculate dependency paths
	var setDepIID bool            // true if all dependencies should have IIDs
	var setDepPath setDepPathFunc // true if given package should have a dependency path

	// configure setDepIID
	switch depPathMode {
	case DepPathModeNone:
		setDepIID = false

	case DepPathModeAffected, DepPathModeAll:
		// set IIDs for all dependencies; a dependency might be referrenced
		// in the path to a vulnerable dependency if it's not vulnerable
		setDepIID = true

		// build index and graph
		index = NewIndex(f.Packages)
		graph = NewGraph(f.Packages, f.Dependencies, index)
	}

	// configure setDepPath
	switch depPathMode {
	case DepPathModeNone:
		// never set dependency path
		setDepPath = func(parser.Package) bool {
			return false
		}

	case DepPathModeAffected:
		// build map of affected packages
		affectedMap := map[parser.Package]bool{}
		for _, aff := range f.Affections {
			affectedMap[aff.Dependency] = true
		}
		// set dependency path if package is affected
		setDepPath = func(pkg parser.Package) bool {
			_, affected := affectedMap[pkg]
			return affected
		}

	case DepPathModeAll:
		// always set dependency path
		setDepPath = func(parser.Package) bool {
			return true
		}
	}

	// initialize dependency converters
	dcs := []DependencyConverter{}
	for _, pkg := range f.Packages {
		dc := DependencyConverter{Package: pkg}
		if setDepIID {
			dc.IID = index.IDOf(pkg)
		}
		if setDepPath(pkg) {
			dc.ShortestPath = graph.PathTo(pkg)
		}
		dcs = append(dcs, dc)
	}

	return &FileConverter{
		File:          f,
		SchemaModel:   schemaModel,
		depConverters: dcs,
	}
}

// FileConverter is used to render a file processed by the scanner
// into a list of vulnerability objects and a dependency file object,
// to be included in the JSON report.
type FileConverter struct {
	File          scanner.File
	SchemaModel   uint
	depConverters []DependencyConverter
}

// DependencyFile generates a dependency file object
// to be included in the JSON report.
func (c FileConverter) DependencyFile() report.DependencyFile {
	// convert to report dependencies
	deps := []report.Dependency{}
	for _, dc := range c.depConverters {
		// Skip dependencies without a version to ensure
		// that the SBOM contains the same components that
		// the dependency report does when schema > 15 is
		// specified.
		//
		// For more info see https://gitlab.com/gitlab-org/gitlab/-/issues/393849
		if dc.dependency().Version == "" {
			continue
		}
		deps = append(deps, dc.dependencyWithPath())
	}

	// return file with dependencies
	return report.DependencyFile{
		Path:           c.File.Path,
		Dependencies:   deps,
		PackageManager: report.PackageManager(c.File.PackageManager),
	}
}

// Vulnerabilities generates a slice of vulnerability objects
// to be included in the JSON report.
func (c FileConverter) Vulnerabilities() []report.Vulnerability {
	// build a map of dependency converters
	dcMap := make(map[parser.Package]DependencyConverter)
	for _, dc := range c.depConverters {
		dcMap[dc.Package] = dc
	}

	// convert vulnerabilities
	vulns := make([]report.Vulnerability, len(c.File.Affections))
	for i, affection := range c.File.Affections {
		depConverter, found := dcMap[affection.Dependency]
		if !found {
			log.Errorf("vulnerable package %s not found in project dependencies", formatPkg(affection.Dependency))
			continue
		}
		c := VulnerabilityConverter{
			Advisory:     affection.Advisory,
			FilePath:     c.File.Path,
			DepConverter: depConverter,
			SchemaModel:  c.SchemaModel,
		}
		vulns[i] = c.Issue()
	}
	return vulns
}
